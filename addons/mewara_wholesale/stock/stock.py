# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time
import datetime
from openerp import tools
from datetime import date
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.float_utils import float_compare, float_round
from math import floor
from operator import itemgetter
from openerp.osv import osv,orm
from matplotlib.testing.jpl_units import min
from matplotlib.dates import minutes

class stock_picking(models.Model):
    _inherit = 'stock.picking'


    @api.one
    @api.depends('picking_type_id')
    def check_pick_type(self):
        if self.picking_type_id.code == 'outgoing':
            self.check_type = True

    @api.one
    @api.depends('driver_id')
    def _compute_driver_lic_no(self):
        if self.driver_id:
            self.driver_lic_no = self.driver_id.otherid

    mode_of_transport = fields.Char(string='Mode of Transport')
    vehicle_id = fields.Many2one('fleet.vehicle', string=" Vehicle No.")
    driver_id = fields.Many2one('hr.employee', string=" Driver Name")
    driver_lic_no = fields.Char(compute='_compute_driver_lic_no', string="Driver Lic. No.", required=True)
    re11_no = fields.Char(string="RE-11 No.")
    re12_no = fields.Char(string="PESO RE-12 Pass No.")
    cust_Lic_no = fields.Char(string="License No.")
    check_type = fields.Boolean(compute="check_pick_type")
    check_date = fields.Boolean("Go ahead")
    order_received = fields.Boolean('Order Received')
    manual_DC = fields.Char("Manual D.C.")
#     invoice_id = fields.Many2one('account.invoice')
    pack_operation_ids = fields.One2many('stock.pack.operation', 'picking_id', states={'done': [('readonly', False)], 'cancel': [('readonly', True)]}, string='Related Packing Operations')

#     @api.v7
#     def _create_invoice_from_picking(self, cr, uid, picking, vals, context=None):
#         ''' This function simply creates the invoice from the given values. It is overriden in delivery module to add the delivery costs.
#         '''
#         invoice_obj = self.pool.get('account.invoice')
#         return invoice_obj.create(cr, uid, vals, context=context)

    @api.cr_uid_ids_context
    def do_enter_transfer_details(self, cr, uid, picking, context=None):
        for picking_id in self.browse(cr, uid, picking):
            if picking_id.picking_type_id.code == 'outgoing' and picking_id.check_date == False:
                main_date = datetime.datetime.strptime(picking_id.min_date, tools.DEFAULT_SERVER_DATETIME_FORMAT) + datetime.timedelta(hours=5, minutes=30)
                if datetime.datetime.strptime(str(main_date), "%Y-%m-%d %H:%M:%S").date() != date.today():
                    raise Warning("Schedule date and and today's date doesn't match ")
        if not context:
            context = {}
      
        context.update({
            'active_model': self._name,
            'active_ids': picking,
            'active_id': len(picking) and picking[0] or False
        })
      
        created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)
        return self.pool['stock.transfer_details'].wizard_view(cr, uid, created_id, context)

    @api.onchange('vehicle_id')
    def onchange_driver(self):
        self.driver_id = self.vehicle_id.dri_id.id

    @api.model
    def _get_invoice_vals(self, key, inv_type, journal_id, move):
        res_val = super(stock_picking, self)._get_invoice_vals(key, inv_type, journal_id, move)
        picking = move.picking_id
        if picking:
            res_val.update({
                            'mode_of_transport': picking.mode_of_transport,
                            'vehicle_id':picking.vehicle_id.id,
                            'driver_id':picking.driver_id.id,
                            'driver_lic_no':picking.driver_lic_no,
                            're11_no':picking.re11_no,
                            're12_no':picking.re12_no,
                            'sale_origin':picking.sale_id.name or ""
                            })
            if picking.partner_id and picking.partner_id.peso_ids:
                res_val.update({
                            'tin_Number':picking.partner_id.peso_ids[0].tin_number,
                            'cust_Lic_no':picking.partner_id.peso_ids[0].license_number,
                            })
        return res_val


class stock_move(models.Model):
    _inherit = "stock.move"

    box_ids = fields.One2many('stock.split.box', 'move_id', string="Boxes")
    picking_type_code = fields.Selection(related = 'picking_type_id.code', string='Picking Type Code', help="Technical field used to display the correct label on print button in the picking view", store=True)

    @api.cr_uid_ids_context
    def _picking_assign(self, cr, uid, move_ids, procurement_group, location_from, location_to, context=None):
        pick_obj = self.pool.get("stock.picking")
        picks = pick_obj.search(cr, uid, [
                ('group_id', '=', procurement_group),
                ('location_id', '=', location_from),
                ('location_dest_id', '=', location_to),
                ('state', 'in', ['draft', 'confirmed', 'waiting'])], context=context)
        if picks:
            pick = picks[0]
        else:
            move = self.browse(cr, uid, move_ids, context=context)[0]
            values = {
                'origin': move.origin,
                'company_id': move.company_id and move.company_id.id or False,
                'move_type': move.group_id and move.group_id.move_type or 'direct',
                'partner_id': move.partner_id.id or False,
                'picking_type_id': move.picking_type_id and move.picking_type_id.id or False,
                're11_no': move.procurement_id.sale_line_id.order_id.cust_ref_no or "",
                'min_date': move.procurement_id.sale_line_id.order_id.exp_ship_date or False,
                'mode_of_transport' : 'By Road'
            }
            pick = pick_obj.create(cr, uid, values, context=context)
        return self.write(cr, uid, move_ids, {'picking_id': pick}, context=context)


class stock_production_lot(models.Model):
    _inherit = "stock.production.lot"
    _order = 'create_date asc'

#

    @api.model
    def create(self, vals):
        if self._context.get('parent_id') and self._context.get('parent_id').get('id'):
            transfer_id = self.env['stock.transfer_details'].browse([self._context.get('parent_id').get('id')])
            if transfer_id.picking_id.picking_type_id.code == 'outgoing':
                raise osv.except_osv(_('Warning!'),
                                _('You can not create serial at the time of sale!'))
        vals.update({'name':vals['name'].upper()})
        return super(stock_production_lot,self).create(vals)

    @api.v7
    def check_stock(self,cr,uid):
        list_id = []
        spl_ids = self.search(cr,uid,[])
        for x in self.browse(cr,uid,spl_ids):
            if x.available_stock >0:
                list_id.append(x.id)
        return {
            'name': 'stock production lot',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.production.lot',
            'domain': [('id', 'in', list_id),],
        }


    @api.v7
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('stock.quant').browse(cr, uid, ids, context=context):
            result[line.lot_id.id] = True
        return result.keys()

    def _get_cases(self):
        box_obj = self.env['stock.split.box']
        for lot in self:
            y = [x.id for x in box_obj.search([('lot_id', '=', lot.id), ('is_transfer_out', '=', False), ('is_manual_transfer', '=', False)])]
            self.case_ids = y
        return True

    available_stock = fields.Float(string='Available Stock', compute='check_stock_lot', default=0.0,store=False)
    case_ids = fields.Many2many('stock.split.box', 'lot_rel_case' , 'lot_id', 'case_id', compute=_get_cases, string="Cases Available")
    batch_date = fields.Date('Batch Date', default=time.strftime("%Y-%m-%d"), required=True)

    @api.model
    @api.depends('case_ids','quant_ids','quant_ids.location_id')
    def check_stock_lot(self):
        '''
            To check the Quantity of products left in this lot
        '''
        cr = self._cr
        uid = self._uid
        available_lots = []
 
        for lot in self:
            total = 0
            if type(lot) != int:
                lot_id = lot[0]
            else:
                lot_id = lot
            quant_ids = lot_id.quant_ids
            if quant_ids:
                incoming_qty = 0
                outgoing_qty = 0
                for quant in quant_ids:
                    if quant.location_id and quant.location_id.usage and quant.location_id.usage == "internal":
                        total += quant.qty
                lot.available_stock = total
        return lot.available_stock

    @api.model
    def check_stock_lot2(self):
        '''
            To check the Quantity of products left in this lot
        '''
        available_lots = []

        for lot in self:
            total = 0
            if type(lot) != int:
                lot_id = lot[0]
            else:
                lot_id = lot
            available_lots.append((lot.id, lot.name +  ' (' + str(int(lot.available_stock or 0)) + ')'))
        return available_lots

# 
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        res = super(stock_production_lot, self).name_search(
             name, args, operator=operator, limit=limit)
        list_id = []
        each_pop = []
        each_pop2 = []
        del_list = []
        if self._context.get('active_id') and isinstance(self._context.get('active_id'),int):
            stdi_id = self.env['stock.transfer_details_items'].browse(self._context.get('active_id'))
            res = self.search([('product_id','=',stdi_id.product_id.id),('name',operator,name)])
            ls_res = list(res)
            for each in ls_res:
                if self._context.get('sourceloc_id') not in [x.location_id.id for x in each.quant_ids]:
                    index = ls_res.index(each)
                    del_list.append(each)
            for each_index in del_list:
                ls_res.remove(each_index)
            res = self.browse([x.id for x in ls_res])
            res = res.name_get()
            if isinstance(self._context.get('active_id'),int):
                source_id = stdi_id.sourceloc_id.id
                if stdi_id and stdi_id.transfer_id and stdi_id.transfer_id.picking_id and stdi_id.transfer_id.picking_id.picking_type_id \
                    and stdi_id.transfer_id.picking_id.picking_type_id.code == "incoming":
                    res = []
                if stdi_id.transfer_id.picking_id and stdi_id.transfer_id.picking_id.picking_type_id \
                    and stdi_id.transfer_id.picking_id.picking_type_id.code == "outgoing":
                    for each in res:
                        lot = self.browse(each[0])
                        for each_quant in lot.quant_ids:
                            if each_quant and each_quant.location_id.id == source_id:
                                each_pop2.append(lot.id)
                    for each in res:
                        lot = self.browse(each[0])
                        if lot.available_stock <= 0:
                            each_pop.append(each)
                    for each_one in each_pop:
                        res.remove(each_one)
        new_res = [x[0] for x in res]
        new_self = self.browse(new_res)
        res = new_self.check_stock_lot2()
        return res
# 
    @api.multi
    def name_get(self):
        result = []
        for lot in self:
            result.append((lot.id, "%s (%s)" %(lot.name,int(lot.available_stock or 0))))
        return result


class stock_quant(models.Model):
    _inherit = 'stock.quant'

    def _quants_get_order(self, cr, uid, location, product, quantity, domain=[], orderby='in_date', context=None):
        if context is None:
            context = {}
        domain += location and [('location_id', 'child_of', location.id)] or []
        domain += [('product_id', '=', product.id)]
        if context.get('force_company'):
            domain += [('company_id', '=', context.get('force_company'))]
        else:
            domain += [('company_id', '=', self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id)]
        res = []
        offset = 0
        while float_compare(quantity, 0, precision_rounding=product.uom_id.rounding) > 0:
            quants = self.search(cr, uid, domain, order=orderby, limit=10, offset=offset, context=context)
            if not quants:
                res.append((None, quantity))
                break
            for x in product.packaging_ids:
                pack_qty = x.qty
                break
            if pack_qty and product.can_be_split:
                remain = quantity % pack_qty
                quotion = floor(quantity / pack_qty)
                if quotion > 0 or remain > 0:
                    full_domain = domain+[('qty','>=',pack_qty)]
                    full_quants = self.search(cr, uid, full_domain, order=orderby, limit=10, offset=offset, context=context)
                    half_domain = domain+[('qty','<',pack_qty)]
                    half_quants = self.search(cr, uid, half_domain, order=orderby, limit=10, offset=offset, context=context)
                    used_quants = []
                    remain_quants = []
                    if len(full_quants) >= quotion:
                        for fquant in self.browse(cr, uid, full_quants, context=context):
                            rounding = product.uom_id.rounding
                            if float_compare(quantity, abs(fquant.qty), precision_rounding=rounding) >= 0:
                                res += [(fquant, abs(fquant.qty))]
                                quantity -= abs(fquant.qty)
                                used_quants.append(fquant.id)
                                if quantity <= 0:
                                    break
                        remain_quants = [x for x in full_quants if x not in used_quants]
                        all_remain_quants = half_quants + remain_quants
                        if all_remain_quants and quantity:
                            for aquant in self.browse(cr, uid, all_remain_quants, context=context):
                                rounding = product.uom_id.rounding
                                if float_compare(quantity, 0.0, precision_rounding=rounding) != 0:
                                    res += [(aquant, quantity)]
                                    quantity -= abs(aquant.qty)
                                    if quantity <= 0:
                                        break
            else:
                for quant in self.browse(cr, uid, quants, context=context):
                    rounding = product.uom_id.rounding
                    if float_compare(quantity, abs(quant.qty), precision_rounding=rounding) >= 0:
                        res += [(quant, abs(quant.qty))]
                        quantity -= abs(quant.qty)
                    elif float_compare(quantity, 0.0, precision_rounding=rounding) != 0:
                        res += [(quant, quantity)]
                        quantity = 0
                        break
            offset += 10
        return res


class stock_pack_operation(models.Model):
    _inherit = 'stock.pack.operation'


    used_cases_ids = fields.Many2many('stock.split.box', 'lot_relation_case' , 'pack_id', 'case_id',string="Used Cases")
    is_manual_entry = fields.Boolean("Manual Entry")
    rep_man_case = fields.Boolean("Manual Entry")

    @api.multi
    def write(self,vals):
        lst_lot_id = []
        if vals.get('is_manual_entry'):
            for each in vals.get('used_cases_ids')[0][2]:
                lst_lot_id.append(each)
            if self.product_qty != len(lst_lot_id):
                raise osv.except_osv(_('Configuration Error!'),
                            _('Quantity and use cases are not equal'))
            lot_rec_ids = self.env['stock.split.box'].browse(lst_lot_id)
            lot_rec_ids.write({'is_transfer_out':True})
        res = super(stock_pack_operation, self).write(vals)
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
