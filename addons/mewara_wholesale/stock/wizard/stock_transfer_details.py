# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning


class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'


    @api.one
    @api.constrains('item_ids')
    def check_qty(self):
        picking_obj = self.env['stock.picking']
        if self._context.get('active_id'):
            picking = picking_obj.browse(self._context.get('active_id'))
            type = picking.picking_type_id and picking.picking_type_id.code
        if type == 'outgoing':
            lots = [x.lot_id for x in self.item_ids if x.lot_id.id]
            for lot in lots:
                effective_lines = []
                for item in self.item_ids:
                    if item.lot_id == lot:
                        effective_lines.append(item)
                effective_total = sum([x.quantity for x in effective_lines])
                available_stock = lot.available_stock or 0.0
                if effective_total > available_stock:
                    raise Warning(_('Available Quantity in Batch ' + lot.name + ' is ' + str(available_stock)))

    def get_item(self, op, split_qty=0.0):
        return {
#             'packop_id': op.id,
            'is_splitable': op.product_id and op.product_id.can_be_split and True or False,
            'product_id': op.product_id.id,
            'product_uom_id': op.product_uom_id.id,
            'quantity': split_qty or op.product_qty,
            'package_id': op.package_id.id,
            'lot_id': op.lot_id.id,
            'sourceloc_id': op.location_id.id,
            'destinationloc_id': op.location_dest_id.id,
            'result_package_id': op.result_package_id.id,
            'date': op.date,
            'owner_id': op.owner_id.id,
        }

    def default_get(self, cr, uid, fields, context=None):
        all_def_box = ()
        box_obj = self.pool.get('stock.split.box')
        if context is None: context = {}
        res = super(stock_transfer_details, self).default_get(cr, uid, fields, context=context)
        picking_ids = context.get('active_ids', [])
        active_model = context.get('active_model')
 
        if not picking_ids or len(picking_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        assert active_model in ('stock.picking'), 'Bad context propagation'
        picking_id, = picking_ids
        picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
        items = []
        packs = []
        split_qty = 0
        if not picking.pack_operation_ids:
            picking.do_prepare_partial()
        for op in picking.pack_operation_ids:
            if not op.product_id.packaging_ids:
                raise Warning(_('Configuration Error!'),
                    _('Please provide packaging qty in product form '))
            else:
                split_qty = op.product_id.packaging_ids[0] and op.product_id.packaging_ids[0].qty or 1
            if split_qty and op.product_qty > split_qty:
                div_mode = divmod(int(op.product_qty), (split_qty))
                for qty in range(int(div_mode[0])):
                    item = self.get_item(op, split_qty)
                    if op.product_id:
                        items.append(item)
                    elif op.package_id:
                        packs.append(item)
                if div_mode[1] > 0:
                    item = self.get_item(op, div_mode[1])
                    if op.product_id:
                        items.append(item)
                    elif op.package_id:
                        packs.append(item)
            else:
                item = self.get_item(op)
                if op.product_id:
                    if op.lot_id:
                        def_boxes = box_obj.search(cr, uid, [('lot_id','=',op.lot_id.id),('is_transfer_out', '=', False), ('is_manual_transfer', '=', False)])
                        all_def_box = (6,0,def_boxes)
                        item['box_ids'] = [all_def_box]
#                         for trnsfer_id in self.browse(cr, uid, ids, context=context):
#                             print "\n\n trnsfer_id--->",trnsfer_id
#                             self.write(cr, uid, [trnsfer_id.id],{'box_ids':all_def_box})
                    items.append(item)
                elif op.package_id:
                    packs.append(item)
        res.update(item_ids=items)
        res.update(packop_ids=packs)
        return res


    @api.one
    def do_detailed_transfer(self):
        rem_lot_cases = []
        processed_ids = []
        obj_prod_lot = self.env['stock.production.lot']
        box_obj = self.env['stock.split.box']
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                #Work of manage boxes method
                if prod.packop_id:
                    packop_id = prod.packop_id
                    prod.packop_id.write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
                if prod.lot_id:
                    if prod.product_id.can_be_split:
                        if self.picking_id.picking_type_id.code == "incoming":
                            if prod.box_ids and not prod.manual_case_entry:
                                if not prod.product_id.packaging_ids:
                                    raise Warning(_('Configuration Error!'),
                                        _('Please provide packaging qty in product form '))
                                if prod.product_id.packaging_ids[0].qty == prod.quantity:
                                    if prod.lot_id and prod.transfer_id:
                                        box_ids = box_obj.search([('lot_id', '=', prod.lot_id.id)])
                                        [x.write({'is_true_in':True,'is_transfer_in':True}) for x in prod.box_ids]
                                elif prod.quantity == len(prod.box_ids):
                                    prod_lot_cases = box_obj.search([('lot_id', '=', prod.lot_id.id), ('is_transfer_in', '=', False),
                                              ('is_transfer_out', '=', False), ('is_true_in', '=', True)])
                                    del_ids = prod_lot_cases - prod.box_ids
                                     #we need to delete all the other cases that are not used, from the serial
                                    [x.unlink() for x in del_ids ]
                                    [x.write({'is_true_in':True,'is_transfer_in':True}) for x in prod.box_ids]
                                else:
                                    raise Warning(_('Configuration Error!'),
                                        _('Please assign cases to lot ' + prod.lot_id.name + ' . '))
                        elif self.picking_id.picking_type_id.code == "outgoing" :
                            if prod.box_ids and not prod.manual_case_entry:
                                if prod.product_id.packaging_ids[0].qty == prod.quantity and prod.lot_id.available_stock == prod.quantity:
                                    box_ids = box_obj.search([('lot_id', '=', prod.lot_id.id)])
                                    [x.write({'is_true_out':True}) for x in prod.box_ids]
                                    [x.write({'is_true_out':False}) for x in box_ids - prod.box_ids]
                                    box_ids_lt = [x.id for x in prod.box_ids]
                                    packop_id.write({'used_cases_ids':[(6,0,box_ids_lt)]})
                                    prod.box_ids.write({'is_transfer_out' : True})
                                elif prod.quantity == len(prod.box_ids):
                                    prod_lot_cases = box_obj.search([('lot_id', '=', prod.lot_id.id)])
                                    box_ids_lt = [x.id for x in prod.box_ids]
                                    packop_id.write({'used_cases_ids':[(6,0,box_ids_lt)]})
                                    [x.write({'is_transfer_out':True}) for x in prod.box_ids]
                                else:
                                    raise Warning(_('Configuration Error!'),
                                    _('Please assign cases to lot ' + prod.lot_id.name + ' . '))
                            if prod.manual_case_entry:
                                box_ids = box_obj.search([('lot_id', '=', prod.lot_id.id), ('is_transfer_in', '=', True)])
                                box_ids_lt_2 = [x.id for x in prod.box_ids]
                                packop_id.write({'used_cases_ids':[(6,0,box_ids_lt_2)],'rep_man_case':True})
#                             box_ids.write({'is_manual_transfer' : True})
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        for packop in packops:
            packop.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        return True


class stock_transfer_details_items(models.TransientModel):
    _inherit = 'stock.transfer_details_items'



    is_splitable = fields.Boolean("Can be split")
    lot_id = fields.Many2one('stock.production.lot', 'Batch No')
    manual_case_entry = fields.Boolean('Manual Case Entry')
    box_ids = fields.Many2many('stock.split.box', 'trans_rel_box', 'box_id', 'transfer_id', 'Cases')
    manual_case_entry = fields.Boolean('Manual Case Entry')



    @api.onchange('manual_case_entry')
    def _onchange_manual_case_entry(self):
        if self._context.get('active_id'):
            item = self.browse(self._context.get('active_id'))
            item.write({'manual_case_entry':self.manual_case_entry})

    @api.multi
    def onchange_lot(self, product_id, lot_id):
        product_id = self.env['product.product'].browse(product_id)
        case_ids = []
        type = ""
        box_obj = self.env['stock.split.box']
        if lot_id and product_id and product_id.can_be_split:
            box_qty = product_id.packaging_ids[0] and product_id.packaging_ids[0].qty or 1
            if self._context.get('active_id') and type(self._context.get('active_id') == int):
                line = self.browse(self._context.get('active_id'))
                type = line.transfer_id.picking_id and line.transfer_id.picking_id.picking_type_id.code
            if self._context.get('parent_id') and self._context.get('parent_id').get('id'):
                transfer_id = self.env['stock.transfer_details'].browse([self._context.get('parent_id').get('id')])
                type = transfer_id.picking_id.picking_type_id.code
            if type == "incoming":
                box_ids = [(box_obj.create({'name' : str(x), 'lot_id': lot_id, 'is_true_in':True})).id for x in  range(1, int(box_qty + 1))]
                return {'value': {'box_ids':[(6,0,box_ids)]}}
            if type == "outgoing":
                box_ids = box_obj.search([('lot_id', '=', lot_id), ('is_true_in', '=', True), ('is_transfer_out', '=', False)])
                case_ids = [x.id for x in box_ids]
                return {'value': {'box_ids':[(6,0,case_ids)]}}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
