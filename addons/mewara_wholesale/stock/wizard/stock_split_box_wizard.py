# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime


class stock_split_box(models.Model):
    _name = 'stock.split.box'

    name = fields.Char('Name')
    is_true_in = fields.Boolean('Select for receive')
    is_true_out = fields.Boolean('Select for deliver')
    lot_id = fields.Many2one('stock.production.lot', string="Serial Number")
    move_id = fields.Many2one('stock.move', string="Stock Move")
    is_transfer_in = fields.Boolean('Received')
    is_transfer_out = fields.Boolean('Delivered')
    is_manual_transfer = fields.Boolean('Manual transfered case')

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('lot_id'):
            case_ids = self.search([('lot_id','=',self._context.get('lot_id')),('name',operator,name)])
            return case_ids.name_get()
        return super(stock_split_box, self).name_search(name, args, operator=operator, limit=limit)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: