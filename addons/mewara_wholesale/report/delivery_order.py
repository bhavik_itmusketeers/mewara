    # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import api, models
import math
import time
from datetime import datetime


class form_re(models.AbstractModel):
    _name = 'report.mewara_wholesale.delivery_order_template'


    t1 = 0
    t2 = 0

    @api.multi
    def render_html(self,data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('mewara_wholesale.delivery_order_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'get_date':self._get_date,
            'get_qty': self._get_qty,
            'get_p6_qty':self._get_p6_qty,
            'get_sum_one':self._get_sum_one,
            'get_sum_two':self._get_sum_two,
            'peso_no':self._peso_no,
            }
        return report_obj.render('mewara_wholesale.delivery_order_template', docargs)


    def _get_date(self,date_time):
        if date_time:
            date_time = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
            inv_date_raw = datetime.strptime(str(date_time.date()), '%Y-%m-%d')
            i_date = datetime.strftime(inv_date_raw, '%d-%m-%Y')
        return i_date

    def _get_prod_categ(self,date_time):
        if date_time:
            date_time = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
            inv_date_raw = datetime.strptime(str(date_time.date()), '%Y-%m-%d')
            i_date = datetime.strftime(inv_date_raw, '%d-%m-%Y')
        return i_date


    def _get_qty(self,pack_id):
        if pack_id:
            if pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_class2')[1]:
                self.t1 = self.t1 + pack_id.product_qty * 25
                return pack_id.product_qty*25
            elif pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_class6')[1]:
                self.t1 = self.t1 + pack_id.product_qty
                return pack_id.product_qty
            elif pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_common')[1]:
                self.t1 = self.t1 + pack_id.product_qty
                return pack_id.product_qty
            else:
                return ""

    def _get_p6_qty(self,pack_id):
        if pack_id:
            if pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_class2')[1]:
                self.t2 = self.t2 + pack_id.product_qty
                return pack_id.product_qty
            elif pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_class6')[1]:
                if isinstance((float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty)), int):
                    self.t2 = self.t2 + pack_id.product_qty / pack_id.product_id.packaging_ids[0].qty
                    return pack_id.product_qty / pack_id.product_id.packaging_ids[0].qty
                else:
                    self.t2 = self.t2 + math.ceil(float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty))
                    return math.ceil(float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty))
            elif pack_id.product_id.categ_id.id == self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'product_category_common')[1]:
                if isinstance((float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty)), int):
                    self.t2 = self.t2 + pack_id.product_qty / pack_id.product_id.packaging_ids[0].qty
                    return pack_id.product_qty / pack_id.product_id.packaging_ids[0].qty
                else:
                    self.t2 = self.t2 + math.ceil(float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty))
                    return math.ceil(float(pack_id.product_qty) / float(pack_id.product_id.packaging_ids[0].qty))
            else:
                self.t2 = self.t2 + pack_id.product_qty
                return pack_id.product_qty


    def _get_sum_one(self):
        return self.t1

    def _get_sum_two(self):
        return self.t2

    def _peso_no(self,peso_line):
        if peso_line:
            st = ''
            for peso_id in peso_line:
                st = st + peso_id.license_number + ","
            st = st[:-1]
            return st

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: