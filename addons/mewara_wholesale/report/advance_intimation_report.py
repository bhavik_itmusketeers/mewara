    # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import api, models
import time
from datetime import datetime


class poset_report(models.AbstractModel):
    _name = 'report.mewara_wholesale.intimation_template'

    sr_no = 0

    @api.multi
    def render_html(self,data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('mewara_wholesale.intimation_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'get_number': self._get_number,
            'get_total_qty':self._get_total_qty,
            'get_date':self._get_date,
            'get_date2':self._get_date2,
            'license_no':self._license_no
        }
        return report_obj.render('mewara_wholesale.intimation_template', docargs)

    def _get_number(self):
        if self.sr_no:
            self.sr_no+=1
        return self.sr_no

    def _get_total_qty(self):
        qty = 0
        order_id = self.env['sale.order'].browse([self._ids[0]])
        for line in order_id.order_line:
            qty += (line.product_uom_qty or 0)
        return qty

    def _get_date(self,date_time):
        if date_time:
            date_time = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
            inv_date_raw = datetime.strptime(str(date_time.date()), '%Y-%m-%d')
            i_date = datetime.strftime(inv_date_raw, '%d-%m-%Y')
            return i_date

    def _get_date2(self,re11_date):
        if re11_date:
            date_format = datetime.strptime(re11_date, "%Y-%m-%d").strftime("%d-%m-%Y")
            return date_format

    def _license_no(self,peso_line):
        st = ''
        if peso_line:
            for peso_id in peso_line:
                st = st + (peso_id.license_number or '') + ","
            st = st[:-1]
            return st

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: