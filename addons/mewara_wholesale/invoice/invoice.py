# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import fields, models, api, _


class account_invoice(models.Model):
    _inherit = 'account.invoice'

    @api.v7
    def fields_view_get(self, cr, user, view_id = None, view_type = 'form',
        context = None, toolbar = False, submenu = False):
        result = super(account_invoice, self).fields_view_get(cr, user, view_id,
                        view_type, context, toolbar, submenu)
        for elm in result['toolbar']['print']:
            if elm.get('string') == "Invoices":
                result['toolbar']['print'].remove(elm)
        return result

    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'mewara_wholesale.tax_invoice_template')

    @api.one
    @api.depends('driver_id')
    def _compute_driver_lic_no(self):
        if self.driver_id:
            self.driver_lic_no = self.driver_id.otherid

    mode_of_transport = fields.Char(string='Mode of Transport', default="By Road")
    vehicle_id = fields.Many2one('fleet.vehicle', string=" Vehicle No.")
    driver_id = fields.Many2one('hr.employee', string=" Driver Name")
    driver_lic_no = fields.Char(compute='_compute_driver_lic_no', string="Driver Lic. No.")
    re11_no = fields.Char(string="RE-11 No.")
    re12_no = fields.Char(string="PESO RE-12 Pass No.")
    cust_Lic_no = fields.Char(string="License No.")
    tin_Number = fields.Char(string="VAT TIN No")
    sale_origin = fields.Char("Sale Id",readonly=True)

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
                            payment_term=False, partner_bank_id=False, company_id=False):
        result = super(account_invoice, self).onchange_partner_id(type, partner_id, date_invoice=False,
                                            payment_term=False, partner_bank_id=False, company_id=False)
        if partner_id:
            res_partner_obj = self.env['res.partner'].browse(partner_id)
            if res_partner_obj.peso_ids:
                result['value'].update({
                                        'cust_Lic_no': res_partner_obj.peso_ids[0].license_number,
                                        'tin_Number' : res_partner_obj.peso_ids[0].tin_number,
                                        })
        return result

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
