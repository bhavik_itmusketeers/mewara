# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Epillars consultants(free lancers).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models,fields,api,_
from openerp.osv import osv,fields


class stock_transfer_details_items(osv.osv_memory):
    _inherit='stock.transfer_details_items'

    _order='orderby_date asc, id asc'

    def _get_orderby_date(self,cr,uid,ids,field,arg,context=None):
        res=dict.fromkeys(ids,False)
        for item in self.browse(cr,uid,ids,context=context):
            res[item.id]=item.lot_id and item.lot_id.create_date or False
        return res

    _columns={
        'orderby_date': fields.function(_get_orderby_date,type='datetime',
                         string='Orderby Date',
                         store={
                         'stock.transfer_details_items': (lambda self,cr,uid,
                                                          ids,c={}:\
                                                  ids,['lot_id'],10)
                      },)
        }


class stock_quant(osv.osv):
    _inherit = 'stock.quant'

    _order = 'orderby_date asc, id asc'

    def _get_orderby_date(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for quant in self.browse(cr, uid, ids, context=context):
            res[quant.id] = {
                'orderby_date': False,
                'batch_date': False,
            }
            res[quant.id]['orderby_date'] = quant.lot_id and quant.lot_id.create_date or False
            res[quant.id]['batch_date'] = quant.lot_id and quant.lot_id.batch_date or False
        return res

    def apply_removal_strategy(self, cr, uid, location, product, quantity, domain, removal_strategy, context=None):
        if removal_strategy == 'fifo':
            order = 'orderby_date, id'
            return self._quants_get_order(cr, uid, location, product, quantity, domain, order, context=context)
        elif removal_strategy == 'lifo':
            order = 'orderby_date desc, id desc'
            return self._quants_get_order(cr, uid, location, product, quantity, domain, order, context=context)
        raise osv.except_osv(_('Error!'),_('Removal strategy %s not implemented.'%(removal_strategy,)))

    _columns = {
        'orderby_date': fields.function(_get_orderby_date, type = 'datetime',
                         string='Creation Date',
                         multi='date',
                         store={
                         'stock.quant': (lambda self, cr, uid,
                                                          ids, c = {}:\
                                                  ids, ['lot_id'], 10)
                      },),
	'batch_date': fields.function(_get_orderby_date, type = 'date',
                         string='Batch Date',
                        multi='date',
                        store={
                         'stock.quant': (lambda self, cr, uid,
                                                          ids, c = {}:\
                                                  ids, ['lot_id'], 11)
                      },
                                      )
        }

