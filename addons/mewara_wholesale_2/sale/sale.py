# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Epillars consultants(free lancers).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp import workflow
from openerp.osv import osv
from bzrlib import api_minimum_version


class sale_order(models.Model):
    _inherit = 'sale.order'

    @api.v7
    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default['name'] = '/'
        return super(sale_order, self).copy(cr, uid, id, default, context)

    @api.v7
    def create(self, cr, uid, vals, context=None):
        if vals.get('name', '/') == '/':
            vals['name'] = self.pool.get('ir.sequence').next_by_code(
                cr, uid, 'sale.quotation') or '/'
        return super(sale_order, self).create(cr, uid, vals, context=context)

    def action_wait(self, cr, uid, ids, context=None):
        if super(sale_order, self).action_wait(cr, uid, ids, context=context):
            for sale in self.browse(cr, uid, ids, context=None):
                quo = sale.name
                self.write(cr, uid, [sale.id], {
                    'origin': quo,
                    'name': self.pool.get('ir.sequence').next_by_code(
                        cr, uid, 'sale.order')
                })
        return True
