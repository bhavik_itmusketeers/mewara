# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
import datetime
import time
from openerp.osv import osv
from datetime import date


class res_users(models.Model):
    _inherit = 'res.users'

    sale_validation = fields.Boolean("Sales Order Confirmation Restriction")


class res_company(models.Model):
    _inherit = 'res.company'

    report_image = fields.Binary("Choose Report image")
    peso_ids = fields.One2many('peso.lines','lines_id')
    tin_number = fields.Char("GST No.")
    header_footer = fields.Boolean("Skip header and footer")
    magazine_at = fields.Char("Magazine")
    taluka = fields.Char("Taluka")
    district = fields.Char("District")
    font_size = fields.Char("Company font size")


class res_partner(models.Model):
    _inherit = 'res.partner'
    
    @api.multi
    def fetch_all(self):
        list_move_ids = []
        account_obj = self.env['account.account']
        account_ids = account_obj.search([('parent_id', 'child_of', [1])])
        all_acc_ids = [x.id for x in account_ids]
        obj_move = self.env['account.move.line']
        query = obj_move._query_get(obj='l')
        query += "AND l.account_id IN " + str(tuple(all_acc_ids))
        for each_self in self:
            if each_self.id:
                acc_id = [each_self.property_account_receivable.id,each_self.property_account_payable.id]
                account_ids = str(tuple(acc_id))
                each_self._cr.execute(
                "SELECT l.id " \
                "FROM account_move_line l " \
                "LEFT JOIN account_journal j " \
                    "ON (l.journal_id = j.id) " \
                "LEFT JOIN account_account acc " \
                    "ON (l.account_id = acc.id) " \
                "LEFT JOIN res_currency c ON (l.currency_id=c.id)" \
                "LEFT JOIN account_move m ON (l.move_id =m.id)" \
                "WHERE l.partner_id = " +(str(each_self.id))+ \
                    " AND l.account_id IN %s"%(account_ids) + " AND " + query + "ORDER BY l.date")
                res = each_self._cr.fetchall()
                for each in res:
                    list_move_ids.append(each[0])
                each_self.journal_item_ids = list_move_ids

    @api.multi
    def fetch_all_button(self):
        list_move_ids = []
        account_obj = self.env['account.account']
        account_ids = account_obj.search([('parent_id', 'child_of', [1])])
        all_acc_ids = [x.id for x in account_ids]
        obj_move = self.env['account.move.line']
        query = obj_move._query_get(obj='l')
        query += "AND l.account_id IN " + str(tuple(all_acc_ids))
        acc_id = [self.property_account_receivable.id,self.property_account_payable.id]
        account_ids = str(tuple(acc_id))
        self._cr.execute(
        "SELECT l.id " \
        "FROM account_move_line l " \
        "LEFT JOIN account_journal j " \
            "ON (l.journal_id = j.id) " \
        "LEFT JOIN account_account acc " \
            "ON (l.account_id = acc.id) " \
        "LEFT JOIN res_currency c ON (l.currency_id=c.id)" \
        "LEFT JOIN account_move m ON (m.id=l.move_id)" \
        "WHERE l.partner_id = " +(str(self.id))+ \
            " AND l.account_id IN %s"%(account_ids) + " AND " + query + \
            "ORDER BY l.date")
        res = self._cr.fetchall()
        for each in res:
            list_move_ids.append(each[0])
#         self.journal_item_ids = list_move_ids
        return {
            'name': _('Account Move Line'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', list_move_ids)]
            }
    
    @api.multi
    @api.depends('balance')
    def compute_total(self):
        for each in self:
            if each.credit or each.debit:
                each.balance = each.credit - each.debit

    @api.one
    @api.depends('blaster_id')
    def blaster_name_change(self):
        if self.blaster_id:
            self.blasterid= self.blaster_id.blasterid
            
        
    #peso_ids = fields.One2many('peso.lines','lines_id', string="PESO Lines")
    res_route_ids = fields.One2many('res.route', 'partner_id', string="Route")
    enable_route =  fields.Boolean(string = 'Enable Intimation')
    history_ids = fields.One2many('history.lines','history_id',readonly=True)
    gstin_number = fields.Char('GST No.')
    blaster_id = fields.Many2one('hr.employee',string= 'Shotfirer/Blaster Name' )
    blasterid = fields.Char(compute="blaster_name_change", string='Shotfirer/Blaster No.')
    balance = fields.Float("Balance",compute='compute_total',inverse='compute_total')
    journal_item_ids = fields.One2many('account.move.line','partner_id',string="Journal Entries", compute='fetch_all')
    
    @api.model
    def delete_rec(self):
        lst_id = []
        today = datetime.datetime.now()
        DD = datetime.timedelta(days=90)
        earlier = today - DD
        self._cr.execute("select id from history_lines where create_date<='%s'"%(str(earlier)))
        total_id = self._cr.dictfetchall()
        for id in total_id:
            lst_id.append(id.get('id'))
        self.env['history.lines'].browse(lst_id).unlink()

    @api.multi
    def write(self, vals):
        objhis_line = self.env['history.lines']
        for part in self:
            his_id = objhis_line.create({'history_date':part.payment_next_action_date,'history_message':part.payment_next_action,'history_id':part.id})
            vals.update({'history_ids':his_id})
        res = super(res_partner,self).write(vals)
        return res

    @api.multi
    def fetch_mail(self):
        mail_ids = ""
        for contact in self.browse(self.id).res_route_ids:
            if contact.sp_email:
                if mail_ids:
                    mail_ids += ',' + contact.sp_email
                else:
                    mail_ids += contact.sp_email
        return mail_ids

    @api.model
    def _display_address(self, address, without_company=False):
        license_nos = " "
        
        address_format = address.country_id.address_format or \
          "%(license_number)s\n%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s"
        args = {
             'license_number': license_nos or '',
            'state_code': address.state_id.code or '',
            'state_name': address.state_id.name or '',
            'country_code': address.country_id.code or '',
            'country_name': address.country_id.name or '',
            'company_name': address.parent_name or '',
        }
        for field in self._address_fields():
            args[field] = getattr(address, field) or ''
        if without_company:
            args['company_name'] = ''
        elif address.parent_id:
            address_format = '%(company_name)s\n' + address_format
        return address_format % args


class res_route(models.Model):
    _name = 'res.route'

    partner_id = fields.Many2one('res.partner', string="Customer")
    district = fields.Char('District Name')
    sp_email = fields.Char('SP Email')


class peso_lines(models.Model):
    _name = 'peso.lines'

    lines_id = fields.Many2one('res.partner', string="Customer")
    license_number = fields.Char('PESO License Number')
    tin_number = fields.Char('TIN Number')


class history_lines(models.Model):
    _name = 'history.lines'

    history_id = fields.Many2one('res.partner', string="Customer")
    history_date = fields.Date('Date')
    history_message = fields.Text('Message')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
