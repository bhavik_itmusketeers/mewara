    # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import pdb
from openerp.addons.web.http import Controller, route, request
from openerp.addons.report.controllers.main import ReportController
from openerp.osv import osv
from openerp import http
import simplejson


class PTReportController(ReportController):
    @route(['/report/download'], type='http', auth="user")
    def report_download(self, data, token):
        print ">>>>>>>>>>>>>>>>>>>>>",data
        if 'intimation_template' in data:
            order_obj = http.request.env['sale.order']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            object = order_obj.browse(int(docids))
            name = object.name
            filename = 'Sale_order-' + object.name
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'poset_template' in data:
            order_obj = http.request.env['purchase.order']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            object = order_obj.browse(int(docids))
            name = object.name
            filename = 'Purchase_order-' + object.name
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'tax_invoice_template' in data:
            order_obj = http.request.env['account.invoice']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            
            doc_ids_2 = [int(x) for x in docids.split(',')]
            filename = 'Invoice_No-'
            for doc_id in doc_ids_2:
                object = order_obj.browse(doc_id)
                if object.number:
                    filename = filename + object.number + ' '
                else:
                    filename = filename + 'Draft'
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'delivery_order_template' in data:
            order_obj = http.request.env['stock.picking']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            object = order_obj.browse(int(docids))
            name = object.name
            filename = 'Picking-' + object.name
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'annexure_tax_invoice_template' in data:
            order_obj = http.request.env['stock.picking']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            object = order_obj.browse(int(docids))
            name = object.name
            filename = 'Annexure_No-' + object.name
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'report_invoice' in data:
            order_obj = http.request.env['account.invoice']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            object = order_obj.browse(int(docids))
            name = object.name
            if object.number:
                    filename = 'Refund-' + object.number + ' '
            else:
                    filename = 'Refund-' + 'Draft'
            response = ReportController().report_download(data, token)
            response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        elif 'report_picking' in data:
            order_obj = http.request.env['stock.picking']
            requestcontent = simplejson.loads(data)
            url, type = requestcontent[0], requestcontent[1]
            assert type == 'qweb-pdf'
            reportname = url.split('/report/pdf/')[1].split('?')[0]
            reportname, docids = reportname.split('/')
            assert docids
            for doc in docids.split(','):
                object = order_obj.browse(int(doc))
                name = object.name
                filename = 'Existing-Picking-' + object.name
                if object.return_order == True:
                    name = object.name
                    filename = 'Return-Order-' + object.name
                response = ReportController().report_download(data, token)
                response.headers.set('Content-Disposition', 'attachment; filename=%s.pdf;' % filename)
            return response
        else:
            response = ReportController().report_download(data, token)
            return response
