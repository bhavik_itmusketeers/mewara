    # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import api, models
import time
from datetime import datetime
from openerp.tools.amount_to_text_en import amount_to_text

class tax_invoice_report(models.AbstractModel):
    _name = 'report.mewara_retail.tax_invoice_template'


    amount_words = ""
    sr_no = 0
    amount_last = 0


    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        user_obj = self.env['res.users']
        user = user_obj.browse(self._uid)
        docargs = {
            'doc_ids': self._ids,
            'doc_model': 'account.invoice',
            'docs': self,
            'order_date': self._order_date,
#             'reference_no':self._reference_no,
            'get_number':self._get_number,
            'get_do_number':self._get_deliver_no,
            'get_deliver_date':self._get_deliver_date,
            'amount_to_text':self._amount_to_text,
            'license_no':self._license_no,
            'tin_no':self._tin_no,
            'ship_add':self._ship_addr,
            'company': user.company_id
        }
        return report_obj.render('mewara_retail.tax_invoice_template', docargs)

    def _order_date(self):
        self._cr.execute("select order_id from sale_order_invoice_rel where invoice_id=%s" % (self._ids[0]))
        order_id = self._cr.dictfetchone()
        inv_id = self.env['account.invoice'].browse([self._ids[0]])
        if order_id and inv_id.invoice_line and order_id.get('order_id'):
            o_date = self.env['sale.order'].browse([order_id.get('order_id')])
            date_time = datetime.strptime(o_date.date_order, "%Y-%m-%d %H:%M:%S")
            inv_date_raw = datetime.strptime(str(date_time.date()), '%Y-%m-%d')
            i_date = datetime.strftime(inv_date_raw, '%d-%m-%Y')
            return i_date
        else:
            return ""


    def _amount_to_text(self, amount, inv_id):
        final_result = ""
        # Currency complete name is not available in res.currency model
        # Exceptions done here (EUR, USD, BRL) cover 75% of cases
        # For other currencies, display the currency code
        if inv_id.currency_id.name.upper() == 'EUR':
            currency_name = 'Euro'
        elif inv_id.currency_id.name.upper() == 'USD':
            currency_name = 'Dollars'
        elif inv_id.currency_id.name.upper() == 'BRL':
            currency_name = 'reais'
        elif inv_id.currency_id.name.upper() == 'INR':
            currency_name = 'Rupees'
            number = '%.2f' % amount
            units_name = inv_id.currency_id.name
        else:
            currency_name = inv_id.currency_id.name
        convert = amount_to_text(amount, currency=currency_name)
        c_list = convert.split(" ");
        c_list[-1] = 'Paisa'
        final = ' '.join(c_list)
        return final

    def _get_number(self):
        self.sr_no += 1
        return self.sr_no

    def _get_deliver_date(self, inv_id):
        date_time = ""
        if inv_id.origin:
            sale_ids = self.env['sale.order'].search([('name', '=', inv_id.sale_origin)])
            if sale_ids:
                if sale_ids.picking_ids:
                    for pick in sale_ids.picking_ids:
                        date_time = datetime.strptime(pick.date, "%Y-%m-%d %H:%M:%S").date()
        return date_time
    
    def _get_deliver_no(self, inv_id):
        number = ""
        if inv_id.origin:
            sale_ids = self.env['sale.order'].search([('name', '=', inv_id.sale_origin)])
            if sale_ids:
                if sale_ids.picking_ids:
                    for pick in sale_ids.picking_ids: 
                        number = pick.name
        return number

    def _license_no(self, peso_line):
        st = ''
        if peso_line:
            for peso_id in peso_line:
                st = st + (peso_id.license_number or '') + ","
            st = st[:-1]
            return st


    def _tin_no(self, peso_line):
        st_join = ''
        if peso_line:
            for peso_id in peso_line:
                if peso_id.tin_number:
                    st_join = st_join + (peso_id.tin_number or '') + ","
            st_join = st_join[:-1]
            return st_join

    def _ship_addr(self, partner):
        st = "" 
        child_partner = self.env['res.partner'].search([('parent_id', '=', partner.id), ('type', '=', 'delivery')], limit=1)
        if child_partner:
            st = (child_partner.street or '') + ", " + (child_partner.street2 or '') + ", "\
             + (child_partner.city or '') + ", " + (child_partner.zip or '') + " - "\
             + (child_partner.state_id.name or '')
            return st or ""

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
