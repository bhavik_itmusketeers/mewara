# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import date
from openerp import SUPERUSER_ID, api
import datetime
from openerp import fields, models, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time
from openerp.osv import osv,orm
from openerp import tools
from openerp.tools.float_utils import float_compare, float_round
from math import floor
from operator import itemgetter


class date_wizard(models.TransientModel):
    _name="date.wizard"
    
    @api.multi
    def go_ahead(self):
        ctx = dict(self._context or {})
        active_id = ctx.get('active_id')
        brw_pick = self.env['stock.picking'].browse(active_id)
        write_pick = brw_pick.write({'check_date':True})
        created_id = self.env['stock.transfer_details'].create( {'picking_id': active_id})
        return created_id.wizard_view()

class stock_picking(models.Model):
    _inherit = 'stock.picking'
    _order= 'date desc'
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'date:day' in groupby:
            orderby = 'date:day DESC' + (orderby and (',' + orderby) or '')
        return super(stock_picking, self).read_group(domain, fields, groupby, offset=0, limit=limit, orderby=orderby, lazy=lazy)

    @api.one
    @api.depends('picking_type_id')
    def check_pick_type(self):
        if self.picking_type_id.code == 'outgoing':
            self.check_type = True
            
    @api.one
    @api.depends('driver_id')
    def _compute_driver_lic_no(self):
        if self.driver_id:
            self.driver_lic_no = self.driver_id.otherid

    @api.one
    @api.depends('re11_no')
    def _compute_shortfire_lic_no(self):
        if self.re11_no:
            #self.re12_no = self.re11_no.otherid
            self.re12_no = self.re11_no.blasterid
            
    @api.onchange('vehicle_id')
    def onchange_driver(self):
        self.driver_id = self.vehicle_id.dri_id.id
    
    
    @api.cr_uid_ids_context
    def do_enter_transfer_details(self, cr, uid, picking, context=None):
        for picking_id in self.browse(cr, uid, picking):
            if picking_id.picking_type_id.code == 'outgoing' and picking_id.check_date == False:
                main_date = datetime.datetime.strptime(picking_id.min_date, tools.DEFAULT_SERVER_DATETIME_FORMAT) + datetime.timedelta(hours=5, minutes=30)
                if datetime.datetime.strptime(str(main_date), "%Y-%m-%d %H:%M:%S").date() != date.today():
                    return{
                         'name':_("Date Validation"),
                            'view_mode': 'form',
                            'view_type': 'form',
                            'res_model': 'date.wizard',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                        }
        if not context:
            context = {}
        context.update({
            'active_model': self._name,
            'active_ids': picking,
            'active_id': len(picking) and picking[0] or False
        })
        created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking) and picking[0] or False}, context)
        return self.pool['stock.transfer_details'].wizard_view(cr, uid, created_id, context)
    
    def do_print_picking(self, cr, uid, ids, context=None):
        '''This function prints the picking list'''
        context = dict(context or {}, active_ids=ids)
        return self.pool.get("report").get_action(cr, uid, ids, 'mewara_retail.report_picking', context=context)
         
    @api.model
    def create(self,vals):
        if vals.get('origin'):
            sale_search = self.env['sale.order'].search([('name' ,'=',vals.get('origin') )])
            if sale_search:
                vals.update({
                    're11_no' : sale_search[0].blaster_id.id,
                    're12_no': sale_search[0].blasterid,
                    })
        res = super(stock_picking, self).create(vals)
        return res
            

    mode_of_transport = fields.Char(string='Mode of Transport')
    vehicle_id = fields.Many2one('fleet.vehicle', string=" Vehicle No.")
    driver_id = fields.Many2one('hr.employee', string=" Driver Name")
    driver_lic_no = fields.Char(compute='_compute_driver_lic_no', string="Driver Lic. No.", required=True)
    re11_no = fields.Many2one('hr.employee',string="Shotfirer Name")
    re12_no = fields.Char(compute='_compute_shortfire_lic_no', string="Shotfirer License no",required=True)
    cust_Lic_no = fields.Char(string="License No.")
    check_type = fields.Boolean(compute="check_pick_type")
    check_date = fields.Boolean("Go ahead")
    order_received = fields.Boolean('Order Received')
    manual_DC = fields.Char("Manual D.C.")
    re13_no = fields.Char("PESO RE-13 Pass No.")
    re13_date = fields.Date("PESO RE-13 Date")
    return_order = fields.Boolean("Return")

    @api.model
    def _get_invoice_vals(self, key, inv_type, journal_id, move):
        res_val = super(stock_picking, self)._get_invoice_vals(key, inv_type, journal_id, move)
        picking = move.picking_id
        if picking:
            res_val.update({
				'partner_id':picking.partner_id.id,
                            'mode_of_transport': picking.mode_of_transport,
                            'vehicle_id':picking.vehicle_id.id,
                            'driver_id':picking.driver_id.id,
                            'driver_lic_no':picking.driver_lic_no,
                            're11_no':picking.re11_no,
                            're12_no':picking.re12_no,
                            'sale_origin':picking.sale_id.name or "",
                            'blaster_id':picking.re11_no.id,
                            })
            if picking.partner_id:
                res_val.update({
                            'tin_Number':picking.partner_id.gstin_number,
                            'cust_Lic_no':picking.re12_no,
                            })
                
        return res_val
    

class stock_move(models.Model):
    _inherit = "stock.move"

    box_ids = fields.One2many('stock.split.box', 'move_id', string="Boxes")
    picking_type_code = fields.Selection(related = 'picking_type_id.code', string='Picking Type Code', help="Technical field used to display the correct label on print button in the picking view", store=True)

    @api.cr_uid_ids_context
    def _picking_assign(self, cr, uid, move_ids, procurement_group, location_from, location_to, context=None):
        pick_obj = self.pool.get("stock.picking")
        picks = pick_obj.search(cr, uid, [
                ('group_id', '=', procurement_group),
                ('location_id', '=', location_from),
                ('location_dest_id', '=', location_to),
                ('state', 'in', ['draft', 'confirmed', 'waiting'])], context=context)
        if picks:
            pick = picks[0]
        else:
            move = self.browse(cr, uid, move_ids, context=context)[0]
            values = {
                'origin': move.origin,
                'company_id': move.company_id and move.company_id.id or False,
                'move_type': move.group_id and move.group_id.move_type or 'direct',
                'partner_id': move.partner_id.id or False,
                'picking_type_id': move.picking_type_id and move.picking_type_id.id or False,
#                 're11_no': move.procurement_id.sale_line_id.order_id.cust_ref_no or "",
                'min_date': move.procurement_id.sale_line_id.order_id.exp_ship_date or False,
                'mode_of_transport' : 'By Road'
            }
            pick = pick_obj.create(cr, uid, values, context=context)
        return self.write(cr, uid, move_ids, {'picking_id': pick}, context=context)
    
    def _get_invoice_line_vals(self, cr, uid, move, partner, inv_type, context=None):
        fp_obj = self.pool.get('account.fiscal.position')
        # Get account_id
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = move.product_id.property_account_income.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_income_categ.id
        else:
            account_id = move.product_id.property_account_expense.id
            if not account_id:
                account_id = move.product_id.categ_id.property_account_expense_categ.id
        fiscal_position = partner.property_account_position
        account_id = fp_obj.map_account(cr, uid, fiscal_position, account_id)

        # set UoS if it's a sale and the picking doesn't have one
        uos_id = move.product_uom.id
        quantity = move.product_uom_qty
#         if move.product_uos:
#             uos_id = move.product_uos.id
#             quantity = move.product_uos_qty
        return {
            'name': move.name,
            'account_id': account_id,
            'product_id': move.product_id.id,
            'uos_id': uos_id,
            'quantity': quantity,
            'price_unit': self._get_price_unit_invoice(cr, uid, move, inv_type),
            'discount': 0.0,
            'account_analytic_id': False,
        }

class stock_production_lot(models.Model):
    _inherit = "stock.production.lot"
    _order = 'create_date asc'


    @api.model
    def create(self, vals):
        if self._context.get('parent_id') and self._context.get('parent_id').get('id'):
            transfer_id = self.env['stock.transfer_details'].browse([self._context.get('parent_id').get('id')])
            if transfer_id.picking_id.picking_type_id.code == 'outgoing':
                raise osv.except_osv(_('Warning!'),
                                _('You can not create serial at the time of sale!'))
        vals.update({'name':vals['name'].upper()})
        return super(stock_production_lot,self).create(vals)
    
    @api.v7
    def check_stock(self,cr,uid):
        list_id = []
        spl_ids = self.search(cr,uid,[])
        for x in self.browse(cr,uid,spl_ids):
            if x.available_stock >0:
                list_id.append(x.id)
        return {
            'name': 'stock production lot',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.production.lot',
            'domain': [('id', 'in', list_id),],
        }
    
    @api.v7
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('stock.quant').browse(cr, uid, ids, context=context):
            result[line.lot_id.id] = True
        return result.keys()

    def _get_cases(self):
        box_obj = self.env['stock.split.box']
        for lot in self:
            y = [x.id for x in box_obj.search([('lot_id', '=', lot.id), ('is_transfer_out', '=', False), ('is_manual_transfer', '=', False)])]
            self.case_ids = y
        return True

    available_stock = fields.Float(string='Available Stock', compute='check_stock_lot', default=0.0)
    case_ids = fields.Many2many('stock.split.box', 'lot_rel_case' , 'lot_id', 'case_id', compute=_get_cases, string="Cases Available")
    batch_date = fields.Date('Batch Date', default=time.strftime("%Y-%m-%d"), required=True)

    @api.model
    @api.depends('case_ids','quant_ids','quant_ids.location_id')
    def check_stock_lot(self):
        '''
            To check the Quantity of products left in this lot
        '''
        cr = self._cr
        uid = self._uid
        available_lots = []

        for lot in self:
            total = 0
            if type(lot) != int:
                lot_id = lot[0]
            else:
                lot_id = lot
            quant_ids = lot_id.quant_ids
            if quant_ids:
                incoming_qty = 0
                outgoing_qty = 0
                for quant in quant_ids:
                    if quant.location_id and quant.location_id.usage and quant.location_id.usage == "internal":
                        total += quant.qty
                lot.available_stock = total
                if total > 0:
                    available_lots.append((lot.id, lot.name +  ' (' + str(int(lot.available_stock or 0)) + ')'))
        return available_lots
    


    @api.model
    def check_stock_lot2(self):
        '''
            To check the Quantity of products left in this lot
        '''
        available_lots = []

        for lot in self:
            total = 0
            if type(lot) != int:
                lot_id = lot[0]
            else:
                lot_id = lot
            available_lots.append((lot.id, lot.name +  ' (' + str(int(lot.available_stock or 0)) + ')'))
        return available_lots

# 
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        res = super(stock_production_lot, self).name_search(
             name, args, operator=operator, limit=limit)
        list_id = []
        each_pop = []
        each_pop2 = []
        del_list = []
        if self._context.get('active_id') and isinstance(self._context.get('active_id'),int):
            stdi_id = self.env['stock.transfer_details_items'].browse(self._context.get('active_id'))
            res = self.search([('product_id','=',stdi_id.product_id.id),('name',operator,name)])
            ls_res = list(res)
            for each in ls_res:
                if self._context.get('sourceloc_id') not in [x.location_id.id for x in each.quant_ids]:
                    index = ls_res.index(each)
                    del_list.append(each)
            for each_index in del_list:
                ls_res.remove(each_index)
            res = self.browse([x.id for x in ls_res])
            res = res.name_get()
            if isinstance(self._context.get('active_id'),int):
                source_id = stdi_id.sourceloc_id.id
                if stdi_id and stdi_id.transfer_id and stdi_id.transfer_id.picking_id and stdi_id.transfer_id.picking_id.picking_type_id \
                    and stdi_id.transfer_id.picking_id.picking_type_id.code == "incoming":
                    if stdi_id.transfer_id.picking_id.return_order == True:
                        for each in res:
                            lot = self.browse(each[0])
                            for each_quant in lot.quant_ids:
                                if each_quant and each_quant.location_id.id == source_id:
                                    each_pop2.append(lot.id)
                        '''for each in res:
                            lot = self.browse(each[0])
                            if lot.available_stock <= 0:
                                each_pop.append(each)
                        for each_one in each_pop:
                            res.remove(each_one)'''
                    else:
                        res = []
                if stdi_id.transfer_id.picking_id and stdi_id.transfer_id.picking_id.picking_type_id \
                    and stdi_id.transfer_id.picking_id.picking_type_id.code == "outgoing":
                    for each in res:
                        lot = self.browse(each[0])
                        for each_quant in lot.quant_ids:
                            if each_quant and each_quant.location_id.id == source_id:
                                each_pop2.append(lot.id)
                    for each in res:
                        lot = self.browse(each[0])
                        if lot.available_stock <= 0:
                            each_pop.append(each)
                    for each_one in each_pop:
                        res.remove(each_one)
        new_res = [x[0] for x in res]
        new_self = self.browse(new_res)
        
        res = new_self.check_stock_lot2()
        return res
# 
    @api.multi
    def name_get(self):
        result = []
        for lot in self:
            result.append((lot.id, "%s (%s)" %(lot.name,int(lot.available_stock or 0))))
        return result
    
    
class stock_quant(models.Model):
    _inherit = 'stock.quant'

    def _quants_get_order(self, cr, uid, location, product, quantity, domain=[], orderby='in_date', context=None):
        if context is None:
            context = {}
        domain += location and [('location_id', 'child_of', location.id)] or []
        domain += [('product_id', '=', product.id)]
        if context.get('force_company'):
            domain += [('company_id', '=', context.get('force_company'))]
        else:
            domain += [('company_id', '=', self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id)]
        res = []
        offset = 0
        while float_compare(quantity, 0, precision_rounding=product.uom_id.rounding) > 0:
            quants = self.search(cr, uid, domain, order=orderby, limit=10, offset=offset, context=context)
            if not quants:
                res.append((None, quantity))
                break
            for x in product.packaging_ids:
                pack_qty = x.qty
                break
            if pack_qty and product.can_be_split:
                remain = quantity % pack_qty
                quotion = floor(quantity / pack_qty)
                if quotion > 0 or remain > 0:
                    full_domain = domain+[('qty','>=',pack_qty)]
                    full_quants = self.search(cr, uid, full_domain, order=orderby, limit=10, offset=offset, context=context)
                    half_domain = domain+[('qty','<',pack_qty)]
                    half_quants = self.search(cr, uid, half_domain, order=orderby, limit=10, offset=offset, context=context)
                    used_quants = []
                    remain_quants = []
                    if len(full_quants) >= quotion:
                        for fquant in self.browse(cr, uid, full_quants, context=context):
                            rounding = product.uom_id.rounding
                            if float_compare(quantity, abs(fquant.qty), precision_rounding=rounding) >= 0:
                                res += [(fquant, abs(fquant.qty))]
                                quantity -= abs(fquant.qty)
                                used_quants.append(fquant.id)
                                if quantity <= 0:
                                    break
                        remain_quants = [x for x in full_quants if x not in used_quants]
                        all_remain_quants = half_quants + remain_quants
                        if all_remain_quants and quantity:
                            for aquant in self.browse(cr, uid, all_remain_quants, context=context):
                                rounding = product.uom_id.rounding
                                if float_compare(quantity, 0.0, precision_rounding=rounding) != 0:
                                    res += [(aquant, quantity)]
                                    quantity -= abs(aquant.qty)
                                    if quantity <= 0:
                                        break
            else:
                for quant in self.browse(cr, uid, quants, context=context):
                    rounding = product.uom_id.rounding
                    if float_compare(quantity, abs(quant.qty), precision_rounding=rounding) >= 0:
                        res += [(quant, abs(quant.qty))]
                        quantity -= abs(quant.qty)
                    elif float_compare(quantity, 0.0, precision_rounding=rounding) != 0:
                        res += [(quant, quantity)]
                        quantity = 0
                        break
            offset += 10
              
        return res
    
    def quants_reserve(self, cr, uid, quants, move, link=False, context=None):
        '''This function reserves quants for the given move (and optionally given link). If the total of quantity reserved is enough, the move's state
        is also set to 'assigned'

        :param quants: list of tuple(quant browse record or None, qty to reserve). If None is given as first tuple element, the item will be ignored. Negative quants should not be received as argument
        :param move: browse record
        :param link: browse record (stock.move.operation.link)
        '''
        toreserve = []
        reserved_availability = move.reserved_availability
        #split quants if needed
        for quant, qty in quants:
            if qty <= 0.0 or (quant and quant.qty <= 0.0):
                raise osv.except_osv(_('Error!'), _('You can not reserve a negative quantity or a negative quant.'))
            if not quant:
                continue
            self._quant_split(cr, uid, quant, qty, context=context)
            toreserve.append(quant.id)
            reserved_availability += quant.qty
        #reserve quants
        if toreserve:
            self.write(cr, SUPERUSER_ID, toreserve, {'reservation_id': move.id}, context=context)
            #if move has a picking_id, write on that picking that pack_operation might have changed and need to be recomputed
            if move.picking_id:
                self.pool.get('stock.picking').write(cr, uid, [move.picking_id.id], {'recompute_pack_op': True}, context=context)
        #check if move'state needs to be set as 'assigned'
        rounding = move.product_id.uom_id.rounding
        if float_compare(reserved_availability, move.product_qty, precision_rounding=rounding) == 0 and move.state in ('confirmed', 'waiting')  :
            self.pool.get('stock.move').write(cr, uid, [move.id], {'state': 'assigned'}, context=context)
        elif context.get('return_picking') and float_compare(reserved_availability, move.product_qty, precision_rounding=rounding) < 0 and move.state in ('confirmed', 'waiting')  :
            self.pool.get('stock.move').write(cr, uid, [move.id], {'state': 'assigned'}, context=context)
        elif float_compare(reserved_availability, 0, precision_rounding=rounding) > 0 and not move.partially_available:
            self.pool.get('stock.move').write(cr, uid, [move.id], {'partially_available': True}, context=context)


class stock_pack_operation(models.Model):
    _inherit = 'stock.pack.operation'


    used_cases_ids = fields.Many2many('stock.split.box', 'lot_relation_case' , 'pack_id', 'case_id',string="Used Cases")
    is_manual_entry = fields.Boolean("Manual Entry")
    rep_man_case = fields.Boolean("Manual Entry")

    @api.multi
    def write(self,vals):
        lst_lot_id = []
        if vals.get('is_manual_entry'):
            for each in vals.get('used_cases_ids')[0][2]:
                lst_lot_id.append(each)
            if self.product_qty != len(lst_lot_id):
                raise osv.except_osv(_('Configuration Error!'),
                            _('Quantity and use cases are not equal'))
            lot_rec_ids = self.env['stock.split.box'].browse(lst_lot_id)
            lot_rec_ids.write({'is_transfer_out':True})
        res = super(stock_pack_operation, self).write(vals)
        return res


class stock_return_picking(models.Model):
    _inherit = 'stock.return.picking'
    
    def _create_returns(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context.update({'return_picking': True})
        record_id = context and context.get('active_id', False) or False
        move_obj = self.pool.get('stock.move')
        pick_obj = self.pool.get('stock.picking')
        pack_obj = self.pool.get('stock.pack.operation')
        data_obj = self.pool.get('stock.return.picking.line')
        pick = pick_obj.browse(cr, uid, record_id, context=context)
        data = self.read(cr, uid, ids[0], context=context)
        returned_lines = 0
        qty = 0
        # Cancel assignment of existing chained assigned moves
        moves_to_unreserve = []
        for move in pick.move_lines:
            to_check_moves = [move.move_dest_id] if move.move_dest_id.id else []
            while to_check_moves:
                current_move = to_check_moves.pop()
                if current_move.state not in ('done', 'cancel') and current_move.reserved_quant_ids:
                    moves_to_unreserve.append(current_move.id)
                split_move_ids = move_obj.search(cr, uid, [('split_from', '=', current_move.id)], context=context)
                if split_move_ids:
                    to_check_moves += move_obj.browse(cr, uid, split_move_ids, context=context)
            for data_get in data_obj.browse(cr, uid, data['product_return_moves'], context=context):
                if data_get.product_id == move.product_id:
                    qty = move.product_uom_qty - data_get.quantity
                    write_qty_pick = move_obj.write(cr, uid, [move.id], {'product_uom_qty': qty }, context=context)
        for op in pick.pack_operation_ids:
            for data_get in data_obj.browse(cr, uid, data['product_return_moves'], context=context):
                if data_get.product_id == op.product_id and data_get.lot_id == op.lot_id:
                    qty = op.product_qty - data_get.quantity
                    write_qty_pack = pack_obj.write(cr, uid, [op.id], {'product_qty': qty}, context=context)
        if moves_to_unreserve:
            move_obj.do_unreserve(cr, uid, moves_to_unreserve, context=context)
            #break the link between moves in order to be able to fix them later if needed
            move_obj.write(cr, uid, moves_to_unreserve, {'move_orig_ids': False}, context=context)

        #Create new picking for returned products
        pick_type_id = pick.picking_type_id.return_picking_type_id and pick.picking_type_id.return_picking_type_id.id or pick.picking_type_id.id
        new_picking = pick_obj.copy(cr, uid, pick.id, {
            'move_lines': [],
            'picking_type_id': pick_type_id,
            'state': 'draft',
            'origin': pick.name,
            'return_order':True,
        }, context=context)

        for data_get in data_obj.browse(cr, uid, data['product_return_moves'], context=context):
            move = data_get.move_id
            if not move:
                raise osv.except_osv(_('Warning !'), _("You have manually created product lines, please delete them to proceed"))
            new_qty = data_get.quantity
            if new_qty:
                # The return of a return should be linked with the original's destination move if it was not cancelled
                if move.origin_returned_move_id.move_dest_id.id and move.origin_returned_move_id.move_dest_id.state != 'cancel':
                    move_dest_id = move.origin_returned_move_id.move_dest_id.id
                else:
                    move_dest_id = False

                returned_lines += 1
                move_obj.copy(cr, uid, move.id, {
                    'product_id': data_get.product_id.id,
                    'product_uom_qty': new_qty,
                    'product_uos_qty': new_qty ,
                    'picking_id': new_picking,
                    'state': 'draft',
                    'location_id': move.location_dest_id.id,
                    'location_dest_id': move.location_id.id,
                    'origin_returned_move_id': move.id,
                    'procure_method': 'make_to_stock',
                    'restrict_lot_id': data_get.lot_id.id,
                    'move_dest_id': move_dest_id,
                })

        if not returned_lines:
            raise osv.except_osv(_('Warning!'), _("Please specify at least one non-zero quantity."))

        pick_obj.action_confirm(cr, uid, [new_picking], context=context)
        pick_obj.action_assign(cr, uid, [new_picking], context)
        return new_picking, pick_type_id
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


    def default_get(self, cr, uid, fields, context=None):
        """
         To get default values for the object.
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param fields: List of fields for which we want default values
         @param context: A standard dictionary
         @return: A dictionary with default values for all field in ``fields``
        """
        result1 = []
        if context is None:
            context = {}
        res = super(stock_return_picking, self).default_get(cr, uid, fields, context=context)
        record_id = context and context.get('active_id', False) or False
        uom_obj = self.pool.get('product.uom')
        pick_obj = self.pool.get('stock.picking')
        pick = pick_obj.browse(cr, uid, record_id, context=context)
        quant_obj = self.pool.get("stock.quant")
        chained_move_exist = False
        if pick:
            if pick.state != 'done':
                raise osv.except_osv(_('Warning!'), _("You may only return pickings that are Done!"))
 
            for move,no in zip(pick.move_lines,pick.pack_operation_ids):
                if move.move_dest_id:
                    chained_move_exist = True
                #Sum the quants in that location that can be returned (they should have been moved by the moves that were included in the returned picking)
                qty = 0
                quant_search = quant_obj.search(cr, uid, [('history_ids', 'in', move.id), ('qty', '>', 0.0), ('location_id', 'child_of', move.location_dest_id.id)], context=context)
                for quant in quant_obj.browse(cr, uid, quant_search, context=context):
                    if not quant.reservation_id or quant.reservation_id.origin_returned_move_id.id != move.id:
                        qty += quant.qty
                qty = uom_obj._compute_qty(cr, uid, move.product_id.uom_id.id, qty, move.product_uom.id)
                result1.append({'product_id': move.product_id.id, 'quantity': qty, 'move_id': move.id,'lot_id' : no.lot_id.id})
 
            if len(result1) == 0:
                raise osv.except_osv(_('Warning!'), _("No products to return (only lines in Done state and not fully returned yet can be returned)!"))
            if 'product_return_moves' in fields:
                res.update({'product_return_moves': result1})
            if 'move_dest_exists' in fields:
                res.update({'move_dest_exists': chained_move_exist})
        return res
