# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import date, datetime, time, timedelta


class filter_move_ondate_wizard(models.TransientModel):
    _name = 'filter.move.ondate.wizard'

    start_date = fields.Date('Start Date', required=True, default=date.today() + timedelta(days=1))
    end_date = fields.Date('End Date', required=True, default=date.today() + timedelta(days=1))
    picking_type = fields.Selection([('incoming', 'Suppliers'), ('outgoing', 'Customers'), ('internal', 'Internal')], 'Type of Operation', required=True)

    @api.multi
    def get_moves(self):
        view_id = self.env['ir.model.data'].get_object_reference('stock', 'view_move_tree')[1]
        move_ids = self.env['stock.move'].search([('date_expected', '<=', self.end_date + ' 23:59:59'),
                                       ('date_expected', '>=', self.start_date + ' 00:00:00'),('picking_type_code','=',self.picking_type)])
        move_ids = [x.ids for x in move_ids] 
        context = self.env.context.copy()
        context.update({"search_default_by_product":True })
        return {
            'name': _('Stock Moves'),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'view_id':view_id,
            'res_model': 'stock.move',
            'domain': [('id', 'in', move_ids)],
            'context': context,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: