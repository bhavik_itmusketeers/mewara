# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime


class stock_split_box(models.Model):
    _name = 'stock.split.box'

    name = fields.Char('Name')
    is_true_in = fields.Boolean('Select for receive')
    is_true_out = fields.Boolean('Select for deliver')
    lot_id = fields.Many2one('stock.production.lot', string="Serial Number")
    move_id = fields.Many2one('stock.move', string="Stock Move")
    is_transfer_in = fields.Boolean('Received')
    is_transfer_out = fields.Boolean('Delivered')
    is_manual_transfer = fields.Boolean('Manual transfered case')
    
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('lot_id'):
            case_ids = self.search([('lot_id','=',self._context.get('lot_id')),('name',operator,name)])
            return case_ids.name_get()
        return super(stock_split_box, self).name_search(name, args, operator=operator, limit=limit)

class stock_split_box_wizard(models.Model):
    _name = 'stock.split.box.wizard'

    @api.onchange('manual_case_entry')
    def _onchange_manual_case_entry(self):
        item_obj = self.env['stock.transfer_details_items']
        if self._context.get('active_id'):
            item = item_obj.browse(self._context.get('active_id'))
            item.write({'manual_case_entry':self.manual_case_entry})

    def _get_boxes(self):
        type = ''
        if self._context.get('active_id'):
            line = self.env['stock.transfer_details_items'].browse(self._context.get('active_id'))
            type = line.transfer_id.picking_id and line.transfer_id.picking_id.picking_type_id.code
        box_qty = 0
        if self._context.get('product_id'):
            product_id = self.env['product.product'].browse(self._context.get('product_id'))
            box_qty = product_id.packaging_ids and product_id.packaging_ids[0].qty or 1
        box_obj = self.env['stock.split.box']
        lot_id = False
        box_ids = []
        if self._context.get('lot_id'):
            lot_id = self._context.get('lot_id')
            box_ids = box_obj.search([('lot_id', '=', lot_id), ('is_true_in', '=', True), ('is_transfer_out', '=', False)])
            if box_ids:
                box_ids = [x.id for x in box_ids]
        if not box_ids:
            if type == "incoming":
                box_ids = [ (box_obj.create({'name' : 'Case ' + str(x), 'lot_id': lot_id, 'is_true_in':True})).id for x in  range(1, int(box_qty + 1))]
            elif type == "outgoing":
                box_ids = [ (box_obj.create({'name' : 'Case ' + str(x), 'lot_id': lot_id, 'is_true_out':True})).id for x in  range(1, int(box_qty + 1))]
        return box_ids

    box_ids = fields.Many2many('stock.split.box', 'wizard_rel_box', 'box_id', 'wizard_id', 'Boxes', default=_get_boxes)
    manual_case_entry = fields.Boolean('Manual Case Entry')

    @api.multi
    def manage_box(self):
#         OK METHOD
        res_id = False
        type = ''
        transfer_item_obj = self.env['stock.transfer_details_items']
        view = self.env.ref('stock.view_stock_enter_transfer_details')
        lot_id = False
        if self._context.get('lot_id'):
            lot_id = self._context.get('lot_id')
        if self._context.get('active_id'):
            active_id = self._context.get('active_id')
            transfer_item_brw = transfer_item_obj.browse(active_id)
            res_id = transfer_item_brw.transfer_id.id
            type = transfer_item_brw.transfer_id.picking_id and transfer_item_brw.transfer_id.picking_id.picking_type_id.code
        box_obj = self.env['stock.split.box']
        box_ids = box_obj.search([('lot_id', '=', lot_id)])
        if type == "incoming" and not self.manual_case_entry:
            [x.write({'is_true_in':True}) for x in self.box_ids]
#             we need to delete all the other cases that are not used, from the serial
            [x.unlink() for x in box_ids if x not in self.box_ids]
        elif type == "outgoing" and not self.manual_case_entry:
            [x.write({'is_true_out':True}) for x in self.box_ids]
            [x.write({'is_true_out':False}) for x in box_ids - self.box_ids]
        return {
            'name': _('Enter transfer details'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'stock.transfer_details',
            'target': 'new',
            'res_id': res_id ,
            'context': self.env.context,
        }

    @api.multi
    def close_box_wizard(self):
        res_id = False
        transfer_item_obj = self.env['stock.transfer_details_items']
        if self._context.get('active_id'):
            active_id = self._context.get('active_id')
            transfer_item_brw = transfer_item_obj.browse(active_id)
            res_id = transfer_item_brw.transfer_id.id
        return {
            'name': _('Enter transfer details'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'stock.transfer_details',
            'target': 'new',
            'res_id': res_id ,
            'context': self.env.context,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: