# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import fields, models, api, _


class account_invoice(models.Model):
    _inherit = 'account.invoice'
    
    
    def fields_view_get(self, cr, user, view_id = None, view_type = 'form',
        context = None, toolbar = False, submenu = False):
        result = super(account_invoice, self).fields_view_get(cr, user, view_id,
                        view_type, context, toolbar, submenu)
        for elm in result['toolbar']['print']:
            if not context.get('type') =='out_refund':
                if elm.get('string') == "Invoices":
                    result['toolbar']['print'].remove(elm)
            if context.get('type') =='out_refund':
                if elm.get('string') == "Tax invoice report" :
                    result['toolbar']['print'].remove(elm)
        return result
    
    
    @api.one
    @api.depends('blaster_id')
    def _compute_blaster_id_lic_no(self):
        if self.blaster_id:
            self.cust_Lic_no = self.blaster_id.blasterid
    
    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'mewara_retail.tax_invoice_template')
    
    @api.one
    @api.depends('driver_id')
    def _compute_driver_lic_no(self):
        if self.driver_id:
            self.driver_lic_no = self.driver_id.otherid

    mode_of_transport = fields.Char(string='Mode of Transport', default="By Road")
    vehicle_id = fields.Many2one('fleet.vehicle', string=" Vehicle No.")
    driver_id = fields.Many2one('hr.employee', string=" Driver Name")
    driver_lic_no = fields.Char(compute='_compute_driver_lic_no', string="Driver Lic. No.")
    re11_no = fields.Char(string="RE-11 No.")
    re12_no = fields.Char(string="PESO RE-13 Pass No.")
    cust_Lic_no = fields.Char(compute='_compute_blaster_id_lic_no',string="Shotfirer/Blaster Licence No.")
    tin_Number = fields.Char(string="GST No.")
    sale_origin = fields.Char("Sale Id",readonly=True)
    cst_no = fields.Char(string="Cst No")
    blaster_id = fields.Many2one('hr.employee','Shotfirer/Blaster Name')
    partner_shipping_id=fields.Many2one('res.partner', 'Delivery Address', readonly=True,  states={'draft': [('readonly', False)]}, help="Delivery address for current sales order.")
    
    @api.multi
    def action_invoice_sent(self):
        """ Open a window to compose an email, with the edi invoice template
            message loaded by default
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        template = self.env.ref('account.email_template_edi_invoice_mewara', False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        ctx = dict(
            default_model='account.invoice',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
        )
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }
        
    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
                            payment_term=False, partner_bank_id=False, company_id=False):
        result = super(account_invoice, self).onchange_partner_id(type, partner_id, date_invoice=False,
                                            payment_term=False, partner_bank_id=False, company_id=False)
        if partner_id:
            res_partner_obj = self.env['res.partner']
            partner_brw = res_partner_obj.browse(partner_id)
            if partner_brw.child_ids:
                for child in partner_brw.child_ids:
                    if child.type == 'delivery':
                        result['value'].update({
                                    'partner_shipping_id': child.id
                                    })
            #if res_partner_obj.peso_ids:
#                 result['value'].update({
#                                         'cust_Lic_no': res_partner_obj.peso_ids[0].license_number,
#                                         'tin_Number' : res_partner_obj.peso_ids[0].tin_number,
#                                         })
            result['value'].update({
                                    'blaster_id':res_partner_obj.blaster_id,
                                    'cust_Lic_no': res_partner_obj.blasterid,
                                     'tin_Number' : res_partner_obj.gstin_number })
        return result
    
    @api.model
    def create(self,vals):
        if vals.get('origin'):
            sale_order_obj = self.env['sale.order'].search([('name','=',vals.get('origin'))])
            if sale_order_obj:
                if sale_order_obj.picking_ids:
                    picking = sale_order_obj.picking_ids[0]
                    vals.update({
                            'mode_of_transport': picking.mode_of_transport,
                            'vehicle_id':picking.vehicle_id.id,
                            'driver_id':picking.driver_id.id,
                            'driver_lic_no':picking.driver_lic_no,
                            're11_no':picking.re11_no,
                            're12_no':picking.re12_no,
                            'sale_origin':picking.sale_id.name or "",
                            'tin_Number':picking.partner_id.gstin_number,
                            'cust_Lic_no':picking.partner_id.blasterid,
                            'blaster_id':picking.re11_no.id,
                            })
        result = super(account_invoice, self).create(vals)
        return result


'''class account_invoice_TAX(models.Model):
    
    _inherit = 'account.invoice.tax'
    
    @api.v8
    def compute(self, invoice):
        tax_grouped = {}
        currency = invoice.currency_id.with_context(date=invoice.date_invoice or fields.Date.context_today(invoice))
        company_currency = invoice.company_id.currency_id
        for line in invoice.invoice_line:
            taxes = line.invoice_line_tax_id.compute_all(
                (line.price_unit * (1 - (line.discount or 0.0) / 100.0)),
                line.quantity, line.product_id, invoice.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'invoice_id': invoice.id,
                    'name': tax['name'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'base': currency.round(tax['price_unit'] * line['quantity']),
                }
                if invoice.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['ref_base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['ref_tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']
                # If the taxes generate moves on the same financial account as the invoice line
                # and no default analytic account is defined at the tax level, propagate the
                # analytic account from the invoice line to the tax line. This is necessary
                # in situations were (part of) the taxes cannot be reclaimed,
                # to ensure the tax move is allocated to the proper analytic account.
                if not val.get('account_analytic_id') and line.account_analytic_id and val['account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id
                key = (val['tax_code_id'], val['base_code_id'], val['account_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount']+= val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = currency.round(t['base'])
            t['amount'] = currency.round(t['amount'])
            t['base_amount'] = currency.round(t['base_amount'])
            t['tax_amount'] = currency.round(t['tax_amount'])
        print "(*******tax_grouped******eeeeeeeeeeeeeeeeeee************((((((((", tax_grouped
        return tax_grouped'''



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
