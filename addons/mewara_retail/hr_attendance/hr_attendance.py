# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class hr_employee(models.Model):
    _inherit = "hr.employee"
    _description = "Employee"


    otherid = fields.Char('License No.')
    auto_attendance = fields.Boolean("Auto Attendance")
    blasterid = fields.Char('Shotfirer/Blaster Licence No.')

    def attendance_schedular(self, cr, uid, context=None):
        employees = self.search(cr, uid, [('auto_attendance', '=', True)])
        self.attendance_action_change(cr, uid, employees, context)
    
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        args_brw = ()
        if self._context.get('from_driver_id'):
            driver_tag_id = self.env['ir.model.data'].get_object_reference('mewara_retail', 'aces_hr_employee_category_driver')[1]
            employee_ids = self.search([])
            for each_emp in employee_ids:
                categ_ids = [x.id for x in each_emp.category_ids]
                if driver_tag_id in categ_ids:
                    args.append(each_emp.id)
            args_brw = self.browse(args)
            if name:
                recs = self.search([('name', operator, name)] , limit=limit)
                return recs.name_get()
            return args_brw.name_get()
        if self._context.get('blaster'):
            shortfire_tags = self.env['ir.model.data'].get_object_reference('mewara_retail', 'aces_hr_employee_category_shortfire')[1]
            employee_ids = self.search([])
            for each_emp in employee_ids:
                categ_ids = [x.id for x in each_emp.category_ids]
                if shortfire_tags in categ_ids:
                    args.append(each_emp.id)
            args_brw = self.browse(args)
            if name:
                recs = self.search([('name', operator, name)] , limit=limit)
                return recs.name_get()
            return args_brw.name_get()
        if self._context.get('from_purchase'):
            driver_tag_id = self.env['ir.model.data'].get_object_reference('mewara_retail', 'aces_hr_employee_category_driver')[1]
#             shortfire_tags = self.env['ir.model.data'].get_object_reference('mewara_wholesale', 'aces_hr_employee_category_driver')[1]
            employee_ids = self.search([])
            for each_emp in employee_ids:
                categ_ids = [x.id for x in each_emp.category_ids]
                if driver_tag_id in categ_ids:
                    args.append(each_emp.id)
            args_brw = self.browse(args)
            if name:
                recs = self.search([('name', operator, name)] , limit=limit)
                return recs.name_get()
            return args_brw.name_get()
        if self._context.get('from_re11_no'):
            shortfire_tags = self.env['ir.model.data'].get_object_reference('mewara_retail', 'aces_hr_employee_category_shortfire')[1]
            employee_ids = self.search([])
            for each_emp in employee_ids:
                categ_ids = [x.id for x in each_emp.category_ids]
                if shortfire_tags in categ_ids:
                    args.append(each_emp.id)
            args_brw = self.browse(args)
            if name:
                recs = self.search([('name', operator, name)] , limit=limit)
                return recs.name_get()
            return args_brw.name_get()
        res = super(hr_employee,self).name_search( name, args=args, operator='ilike', limit=100)
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: