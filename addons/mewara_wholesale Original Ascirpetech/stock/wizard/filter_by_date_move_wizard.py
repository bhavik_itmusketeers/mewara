# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import date, datetime, time, timedelta


class filter_move_ondate_wizard(models.TransientModel):
    _name = 'filter.move.ondate.wizard'

    start_date = fields.Date('Start Date', required=True, default=date.today() + timedelta(days=1))
    end_date = fields.Date('End Date', required=True, default=date.today() + timedelta(days=1))
    picking_type = fields.Selection([('incoming', 'Suppliers'), ('outgoing', 'Customers'), ('internal', 'Internal')], 'Type of Operation', required=True,default='outgoing')



    @api.multi
    def get_moves(self):
        picking_ids = self.env['stock.picking'].search([('min_date', '<=', self.end_date + ' 23:59:59'),
                                       ('min_date', '>=', self.start_date + ' 00:00:00'),('picking_type_id.code','=',self.picking_type)])
        picking_ids = [x.ids for x in picking_ids] 
        return {
            'name': _('Stock Picking'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'domain': [('id', 'in', picking_ids),],
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: