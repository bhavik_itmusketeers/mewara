    # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import api, models
import time
from datetime import datetime


class report_picking(models.AbstractModel):
    _name = 'report.mewara_wholesale.report_picking'


    @api.multi
    def render_html(self,data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('mewara_wholesale.report_picking')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self.env['stock.picking'].browse(self._ids),
            'get_date':self._get_date,
            'rem_comma' : self._rem_comma,
            'use_cases_no' : self._use_cases_no
        }
        return report_obj.render('mewara_wholesale.report_picking', docargs)


    def _get_date(self,date_time):
        date_time = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
        inv_date_raw = datetime.strptime(str(date_time.date()), '%Y-%m-%d')
        i_date = datetime.strftime(inv_date_raw, '%d-%m-%Y')
        return i_date

    def _rem_comma(self,id):
        return id.name

    def _use_cases_no(self,use_cases):
        st = ''
        for case_id in use_cases:
            if len(case_id.name) == 1:
                st = st + '0' + case_id.name + ",\n"
            else:
                st = st + case_id.name + ",\n"
        st = st[:-1]
        return st

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: