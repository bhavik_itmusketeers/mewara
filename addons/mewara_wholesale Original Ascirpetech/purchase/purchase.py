# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api, _


class purchase_order(models.Model):
    _inherit = 'purchase.order'


    @api.multi
    @api.depends('vehical_id')
    def _compute_vehicle_lic_no(self):
        for each_self in self:
            if each_self.vehical_id:
                each_self.vehical_no = each_self.vehical_id.license_plate
            if each_self.vehical_id.peso_ids:
                each_self.vehical_lic_no = each_self.vehical_id.peso_ids[0].license_number
 
    @api.multi
    @api.depends('driver_id')
    def _compute_driver_lic_no(self):
        for each_self in self:
            if each_self.driver_id:
                each_self.driver_lic_no = each_self.driver_id.otherid

    driver_id = fields.Many2one('hr.employee', string=" Driver Name")
    driver_lic_no = fields.Char(compute='_compute_driver_lic_no', string="Driver Lic. No.", required=True)
    re11_no = fields.Char(string="RE-11 No.")
    re11_date = fields.Date(string="RE-11 Date")
    vehical_id = fields.Many2one('fleet.vehicle',string="Select Vehicle")
    vehical_no = fields.Char(string="Vehicle No",compute = '_compute_vehicle_lic_no')
    vehical_lic_no = fields.Char(string="Vehical License No", compute = '_compute_vehicle_lic_no')


    @api.onchange('vehical_id')
    def onchange_driver(self):
        self.driver_id = self.vehical_id.dri_id.id

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: