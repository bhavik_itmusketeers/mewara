# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################from openerp import fields, models, api, _

from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import fields, models, api, _
import datetime
import time
from datetime import date
# from datetime import datetime

class customer_wizard(models.TransientModel):
    _name = 'customer.wizard'


    start_date = fields.Date('Starting Date')

    @api.multi
    def filter_customer(self):
        today = time.strftime("%Y-%m-%d")
        if self.start_date > today:
            raise except_orm(_('start date is not bigger then today'),_("It is not valid''!"))
        date_conv = str(datetime.datetime.strptime(self.start_date, '%Y-%m-%d'))
        inactive_ids = []
        inactive_invoice = []
        inactive_sales = []
        customer_id = self.env['res.partner'].search([('customer','=',True)])
        for each_cust in customer_id:
            invoice_id = self.env['account.invoice'].search([('partner_id','=',each_cust.id),('state','=','paid'),('date_invoice','>=',self.start_date)])
            if not invoice_id:
                inactive_invoice.append(each_cust.id)
            cust_sale_ordrs = self.env['sale.order'].search([('partner_id','=',each_cust.id),('date_order',">=",date_conv),('state','not in',['draft','sent','cancel','waiting_date'])])
            if not cust_sale_ordrs:
                inactive_sales.append(each_cust.id)
        inactive_ids = list(set(inactive_invoice).intersection(inactive_sales))
        return {
            'name': _('res partner'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'res.partner',
            'domain': [('id', 'in', inactive_ids),],
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: