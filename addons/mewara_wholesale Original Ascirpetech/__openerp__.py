# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Mewara Wholesale',
    'version': '1.0.1',
    'author' : 'Acespritech Solutions Pvt.Ltd.',
    'website': 'http://acespritech.co.in',
    'summary': '',
    'depends': ['base', 'mail', 'email_template', 'account', 'sale',
                'stock', 'stock_account', 'fleet', 'hr', 'hr_attendance',
                'purchase','account_accountant','web','sale_stock'],
    'description': """
    """,
    'data':[
            'security/ir.model.access.csv',
            'mewara_wholesale_data.xml',
            'wizard/customer_wizard.xml',
            'invoice/invoice_view.xml',
            'account/account_view.xml',
            'purchase/purchase_view.xml',
            'sale/sale_view.xml',
            'res/res_view.xml',
            'stock/stock_view.xml',
            'stock/wizard/stock_transfer_details.xml',
            'stock/wizard/stock_split_box_wizard.xml',
            'res/res_data.xml',
            'product/product_view.xml',
            'stock/wizard/filter_by_date_move_wizard_view.xml',
            'hr_attendance/hr_employee_view.xml',
            'hr_attendance/hr_attendance_data.xml',
            'view/css_inherit.xml',
            'view/poset_template.xml',
            'view/intimation_template.xml',
            'view/delivery_order_template.xml',
            'view/tax_invoice_template.xml',
            'view/report_picking.xml',
            'report/layouts.xml',
            'report/poest_report_view.xml',
            'fleet/fleet_view.xml',
            ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
