# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import date, timedelta
import time
from openerp import models, fields, api, _
from openerp import workflow
from openerp.osv import osv



class sale_order(models.Model):
    _inherit = 'sale.order'

    cust_id = 0

    @api.one
    def check_user(self):
        user_id = self.env['res.users'].browse([self._uid])
        if user_id.sale_validation:
            raise osv.except_osv(_('Warning!'), _("Sorry, You don't have access right to confirm sale"))
        else:
            return True

    @api.one
    @api.depends('re11_received')
    def re11_receive(self):
        if self.re11_received:
            self.re11_status = "Received"
        else:
            self.re11_status = "Not Received"

    @api.multi
    def stock_warning(self):
        sum_qty = 0
        for each_line in self.order_line:
            all_quant_ids = self.env['stock.quant'].search([('location_id.id','=',self.warehouse_id.lot_stock_id.id),('product_id','=',each_line.product_id.id)])
            if all_quant_ids:
                for each_quant in all_quant_ids:
                    sum_qty+=each_quant.qty
                if sum_qty < each_line.product_uom_qty:
                    raise osv.except_osv(_('Warning!'), _('Stock is not available!\n So,please select another warehouse.'))
            else:
                raise osv.except_osv(_('Warning!'), _('Product is not available in this warehouse!\n So,please select another warehouse.'))
 
 
    @api.multi
    def view_location(self):
        loc_id = self.env['stock.location'].browse([self.warehouse_id.lot_stock_id.id])
        return {
            'name': _('Stock location'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.quant',
            'context' : {'search_default_internal_loc':1,'search_default_productgroup': 1},
            'domain' : [('location_id','=',loc_id.id)]
        }

    @api.multi
    @api.depends('exp_ship_date')
    def _compute_date(self):
        for each_self in self:
            picking_id = each_self.env['stock.picking'].search([('group_id','=',each_self.procurement_group_id.id)])
            if picking_id:
                for each_id in picking_id:
                    if each_id.state not in 'done':
                        each_id.min_date = each_self.exp_ship_date


    @api.one
    @api.depends('partner_id')
    def _compute_move_lines(self):
        if self.partner_id:
            list_move_ids = []
            account_obj = self.env['account.account']
            account_ids = account_obj.search([('parent_id', 'child_of', [1])])
            all_acc_ids = [x.id for x in account_ids]
            obj_move = self.env['account.move.line']
            query = obj_move._query_get(obj='l')
            query += "AND l.account_id IN " + str(tuple(all_acc_ids))
            for each_self in self:
                acc_id = [each_self.partner_id.property_account_receivable.id,each_self.partner_id.property_account_payable.id]
                account_ids = str(tuple(acc_id))
                each_self._cr.execute(
                "SELECT l.id " \
                "FROM account_move_line l " \
                "LEFT JOIN account_journal j " \
                    "ON (l.journal_id = j.id) " \
                "LEFT JOIN account_account acc " \
                    "ON (l.account_id = acc.id) " \
                "LEFT JOIN res_currency c ON (l.currency_id=c.id)" \
                "LEFT JOIN account_move m ON (m.id=l.move_id)" \
                "WHERE l.partner_id = " +(str(each_self.partner_id.id))+ \
                    " AND l.account_id IN %s"%(account_ids) + " AND " + query + \
                    "ORDER BY l.date")
                res_query = each_self._cr.fetchall()
                for each in res_query:
                    list_move_ids.append(each[0])
            each_self.move_lines = list_move_ids

    @api.multi
    @api.depends('partner_id')
    def compute_total(self):
        for each in self:
            if each.partner_id.credit or each.partner_id.debit:
                each.balance = each.partner_id.credit - each.partner_id.debit

    cust_ref_no = fields.Char('RE-11 No.')
    exp_ship_date = fields.Datetime('Delivery Date',store=True,readonly=False,compute='_compute_date',inverse='_compute_date',required=True,copy=True)
    order_policy = fields.Selection([
                ('manual', 'On Demand'),
                ('picking', 'On Delivery Order'),
                ('prepaid', 'Before Delivery'),
            ],string = 'Create Invoice', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
            help="""This field controls how invoice and delivery operations are synchronized.""")
    move_lines = fields.Many2many('account.move.line', string='Entry Lines',
                                  compute='_compute_move_lines')
    balance = fields.Float("Balance",compute='compute_total', readonly=True)
    re11_received = fields.Boolean('RE-11 Received')
    re11_date = fields.Date("RE-11 Date")
    enable_route = fields.Boolean(related='partner_id.enable_route', store=True)
    re11_status = fields.Char("RE-11 Status", compute='re11_receive')
    confirm_status = fields.Boolean("Confirm status")

    @api.v7
    def default_get(self, cr, uid, fields, context=None):
        res = super(sale_order, self).default_get(cr, uid, fields, context=context)
        if res.get('order_policy'):
            res.update({'order_policy':'picking'})
        return res

    @api.v7
    def route_send_mail(self, cr, uid, ids, context=None):
        ir_model_data = self.pool.get('ir.model.data')
        template_obj = self.pool.get('email.template')
        template_id = ir_model_data.get_object_reference(cr, uid, 'mewara_wholesale', 'email_template_edi_mewara_wholesale')[1]
        for partner in self.browse(cr, uid, ids):
            template_obj.send_mail(cr, uid, template_id, partner.partner_id.id, True, context=None)
        self.signal_workflow(cr, uid, ids, 'quotation_sent')
        self.stock_warning(cr, uid, ids)
        return True


    @api.one
    def intimation_false(self):
        self.stock_warning()
        self.action_button_confirm()
# 
    def action_ship_create(self, cr, uid, ids, context=None):
        """Create the required procurements to supply sales order lines, also connecting
        the procurements to appropriate stock moves in order to bring the goods to the
        sales order's requested location.
    
        :return: True
        """
        self.stock_warning(cr, uid, ids)
        class2_categ_id = self.pool.get('ir.model.data').get_object_reference(cr,uid,'mewara_wholesale', 'product_category_class2')[1]
        class6_categ_id = self.pool.get('ir.model.data').get_object_reference(cr,uid,'mewara_wholesale', 'product_category_class6')[1]
        procurement_obj = self.pool.get('procurement.order')
        sale_line_obj = self.pool.get('sale.order.line')
        frst_gr_id = False
        for order in self.browse(cr, uid, ids, context=context):
            proc_ids = []
            vals = self._prepare_procurement_group(cr, uid, order, context=context)
            if not order.procurement_group_id:
                group_id = self.pool.get("procurement.group").create(cr, uid, vals, context=context)
                frst_gr_id = group_id
                order.write({'procurement_group_id': group_id})
            for line in order.order_line:
                if line.product_id.categ_id.id == class2_categ_id:
                # Try to fix exception procurement (possible when after a shipping exception the user choose to recreate)
                    if line.procurement_ids:
                        # first check them to see if they are in exception or not (one of the related moves is cancelled)
                        procurement_obj.check(cr, uid, [x.id for x in line.procurement_ids if x.state not in ['cancel', 'done']])
                        line.refresh()
                        # run again procurement that are in exception in order to trigger another move
                        proc_ids += [x.id for x in line.procurement_ids if x.state in ('exception', 'cancel')]
                        procurement_obj.reset_to_confirmed(cr, uid, proc_ids, context=context)
                    elif sale_line_obj.need_procurement(cr, uid, [line.id], context=context):
                        if (line.state == 'done') or not line.product_id:
                            continue
                        vals = self._prepare_order_line_procurement(cr, uid, order, line, group_id=order.procurement_group_id.id, context=context)
                        vals['date_planned'] = order.exp_ship_date or ""
                        proc_id = procurement_obj.create(cr, uid, vals, context=context)
                        proc_ids.append(proc_id)
            # Confirm procurement order such that rules will be applied on it
            # note that the workflow normally ensure proc_ids isn't an empty list
            if proc_ids:
                procurement_obj.run(cr, uid, proc_ids, context=context)
            if order.state == 'shipping_except':
                val = {'state': 'progress', 'shipped': False}

                if (order.order_policy == 'manual'):
                    for line in order.order_line:
                        if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                            val['state'] = 'manual'
                            break
                order.write(val)
        for order in self.browse(cr, uid, ids, context=context):
            proc_ids = []
            group_id = self.pool.get("procurement.group").create(cr, uid, vals, context=context)
            for line in order.order_line:
                if line.product_id.categ_id.id != class2_categ_id:
                    self.cust_id = line.order_partner_id
                    vals = self._prepare_procurement_group(cr, uid, order, context=context)
                    order.write({'procurement_group_id': group_id})
                # Try to fix exception procurement (possible when after a shipping exception the user choose to recreate)
                    if sale_line_obj.need_procurement(cr, uid, [line.id], context=context):
                        if (line.state == 'done') or not line.product_id:
                            continue
                        vals = self._prepare_order_line_procurement(cr, uid, order, line, group_id=order.procurement_group_id.id, context=context)
                        vals['date_planned'] = order.exp_ship_date or ""
                        proc_id = procurement_obj.create(cr, uid, vals, context=context)
                        proc_ids.append(proc_id)
            # Confirm procurement order such that rules will be applied on it
            # note that the workflow normally ensure proc_ids isn't an empty list
            if proc_ids:
                procurement_obj.run(cr, uid, proc_ids, context=context)
                if group_id != frst_gr_id:
                    pick_id = self.pool.get('stock.picking').search(cr,uid,[('group_id','=',group_id)])
                    self.pool.get('stock.picking').write(cr,uid,pick_id,{'partner_id':self.cust_id.id})
                if frst_gr_id:
                   do_id = self.pool.get('stock.picking').search(cr,uid,[('group_id','=',frst_gr_id)])
                   if do_id:
                       self.pool.get('stock.picking').write(cr,uid,do_id,{'group_id':group_id})
 
#             if shipping was in exception and the user choose to recreate the delivery order, write the new status of SO
            if order.state == 'shipping_except':
                val = {'state': 'progress', 'shipped': False}
     
                if (order.order_policy == 'manual'):
                    for line in order.order_line:
                        if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                            val['state'] = 'manual'
                            break
                order.write(val)
        return True

    @api.model
    def _prepare_invoice(self, order, lines):
        res_val = super(sale_order, self)._prepare_invoice(order, lines)
        if order.partner_id:
            if order.partner_id.peso_ids:
                res_val.update({
                    'cust_Lic_no': order.partner_id.peso_ids[0].license_number,
                    're11_no': order.cust_ref_no,
                    'tin_Number': order.partner_id.peso_ids[0].tin_number,
                })
        return res_val


class sale_advance_payment_inv(models.Model):
    _inherit = "sale.advance.payment.inv"

    @api.v7
    def _prepare_advance_invoice_vals(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sale_obj = self.pool.get('sale.order')
        ir_property_obj = self.pool.get('ir.property')
        fiscal_obj = self.pool.get('account.fiscal.position')
        inv_line_obj = self.pool.get('account.invoice.line')
        wizard = self.browse(cr, uid, ids[0], context)
        sale_ids = context.get('active_ids', [])
 
        result = []
        for sale in sale_obj.browse(cr, uid, sale_ids, context=context):
            val = inv_line_obj.product_id_change(cr, uid, [], wizard.product_id.id,
                    False, partner_id=sale.partner_id.id, fposition_id=sale.fiscal_position.id)
            res = val['value']
 
            # determine and check income account
            if not wizard.product_id.id :
                prop = ir_property_obj.get(cr, uid,
                            'property_account_income_categ', 'product.category', context=context)
                prop_id = prop and prop.id or False
                account_id = fiscal_obj.map_account(cr, uid, sale.fiscal_position or False, prop_id)
                if not account_id:
                    raise osv.except_osv(_('Configuration Error!'),
                            _('There is no income account defined as global property.'))
                res['account_id'] = account_id
            if not res.get('account_id'):
                raise osv.except_osv(_('Configuration Error!'),
                        _('There is no income account defined for this product: "%s" (id:%d).') % \
                            (wizard.product_id.name, wizard.product_id.id,))
 
            # determine invoice amount
            if wizard.amount <= 0.00:
                raise osv.except_osv(_('Incorrect Data'),
                    _('The value of Advance Amount must be positive.'))
            if wizard.advance_payment_method == 'percentage':
                inv_amount = sale.amount_total * wizard.amount / 100
                if not res.get('name'):
                    res['name'] = self._translate_advance(cr, uid, percentage=True, context=dict(context, lang=sale.partner_id.lang)) % (wizard.amount)
            else:
                inv_amount = wizard.amount
                if not res.get('name'):
                    # TODO: should find a way to call formatLang() from rml_parse
                    symbol = sale.pricelist_id.currency_id.symbol
                    if sale.pricelist_id.currency_id.position == 'after':
                        symbol_order = (inv_amount, symbol)
                    else:
                        symbol_order = (symbol, inv_amount)
                    res['name'] = self._translate_advance(cr, uid, context=dict(context, lang=sale.partner_id.lang)) % symbol_order
 
            # determine taxes
            if res.get('invoice_line_tax_id'):
                res['invoice_line_tax_id'] = [(6, 0, res.get('invoice_line_tax_id'))]
            else:
                res['invoice_line_tax_id'] = False
            inv_line_values = {
                'name': res.get('name'),
                'origin': sale.name,
                'account_id': res['account_id'],
                'price_unit': inv_amount,
                'quantity': wizard.qtty or 1.0,
                'discount': False,
                'uos_id': res.get('uos_id', False),
                'product_id': wizard.product_id.id,
                'invoice_line_tax_id': res.get('invoice_line_tax_id'),
                'account_analytic_id': sale.project_id.id or False,
            }
            inv_values = {
                'name': sale.client_order_ref or sale.name,
                'origin': sale.name,
                'type': 'out_invoice',
                'reference': False,
                'account_id': sale.partner_id.property_account_receivable.id,
                'partner_id': sale.partner_invoice_id.id,
                'invoice_line': [(0, 0, inv_line_values)],
                'currency_id': sale.pricelist_id.currency_id.id,
                'comment': '',
                'payment_term': sale.payment_term.id,
                'fiscal_position': sale.fiscal_position.id or sale.partner_id.property_account_position.id,
                'cust_Lic_no':sale.partner_id.peso_ids.license_number,
                'tin_Number': sale.partner_id.peso_ids.tin_number,
                're11_no':sale.cust_ref_no,
            }
            result.append((sale.id, inv_values))
        return result

class sale_order_line_make_invoice(models.Model):
    _inherit = "sale.order.line.make.invoice"

    @api.v7
    def make_invoices(self, cr, uid, ids, context=None):
        if context is None: context = {}
        res = False
        invoices = {}

    # TODO: merge with sale.py/make_invoice
        def make_invoice(order, lines):
            a = order.partner_id.property_account_receivable.id
            if order.partner_id and order.partner_id.property_payment_term.id:
                pay_term = order.partner_id.property_payment_term.id
            else:
                pay_term = False
            if order.partner_id and order.partner_id.peso_ids:
                inv = {
                    'name': order.client_order_ref or '',
                    'origin': order.name,
                    'type': 'out_invoice',
                    'reference': "P%dSO%d" % (order.partner_id.id, order.id),
                    'account_id': a,
                    'partner_id': order.partner_invoice_id.id,
                    'invoice_line': [(6, 0, lines)],
                    'currency_id' : order.pricelist_id.currency_id.id,
                    'comment': order.note,
                    'payment_term': pay_term,
                    'fiscal_position': order.fiscal_position.id or order.partner_id.property_account_position.id,
                    'user_id': order.user_id and order.user_id.id or False,
                    'company_id': order.company_id and order.company_id.id or False,
                    'date_invoice': fields.date.today(),
                    'tin_Number':order.partner_id.peso_ids[0].tin_number,
                    'cust_Lic_no':order.partner_id.peso_ids[0].license_number,
                    're11_no':order.cust_ref_no,
                    'sale_origin':order.name or ""
                    
                }
            inv_id = self.pool.get('account.invoice').create(cr, uid, inv)
            return inv_id

        sales_order_line_obj = self.pool.get('sale.order.line')
        sales_order_obj = self.pool.get('sale.order')
        for line in sales_order_line_obj.browse(cr, uid, context.get('active_ids', []), context=context):
            if (not line.invoiced) and (line.state not in ('draft', 'cancel')):
                if not line.order_id in invoices:
                    invoices[line.order_id] = []
                line_id = sales_order_line_obj.invoice_line_create(cr, uid, [line.id])
                for lid in line_id:
                    invoices[line.order_id].append(lid)
        for order, il in invoices.items():
            res = make_invoice(order, il)
            cr.execute('INSERT INTO sale_order_invoice_rel \
                    (order_id,invoice_id) values (%s,%s)', (order.id, res))
            sales_order_obj.invalidate_cache(cr, uid, ['invoice_ids'], [order.id], context=context)
            flag = True
            sales_order_obj.message_post(cr, uid, [order.id], body=_("Invoice created"), context=context)
            data_sale = sales_order_obj.browse(cr, uid, order.id, context=context)
            for line in data_sale.order_line:
                if not line.invoiced:
                    flag = False
                    break
            if flag:
                line.order_id.write({'state': 'progress'})
                workflow.trg_validate(uid, 'sale.order', order.id, 'all_lines', cr)

        if not invoices:
            raise osv.except_osv(_('Warning!'), _('Invoice cannot be created for this Sales Order Line due to one of the following reasons:\n1.The state of this sales order line is either "draft" or "cancel"!\n2.The Sales Order Line is Invoiced!'))
        if context.get('open_invoices', False):
            return self.open_invoices(cr, uid, ids, res, context=context)
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
